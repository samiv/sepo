import {WEEKDAYS, SepoNotificationWithId} from './CommonTypes';

export const isToday = (date: Date): boolean => {
    const now = new Date();
    return (
      date.getDate() === now.getDate() &&
      date.getMonth() === now.getMonth() &&
      date.getFullYear() === now.getFullYear()
    );
  };
  
  export const isTomorrow = (date: Date): boolean => {
    const now = new Date();
    let tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
    return (
      date.getDate() === tomorrow.getDate() &&
      date.getMonth() === tomorrow.getMonth() &&
      date.getFullYear() === tomorrow.getFullYear()
    );
  };
  
  export const getDayString = (date: Date): string => {
    if (isToday(date)) {
      return 'Today';
    } else if (isTomorrow(date)) {
      return 'Tomorrow';
    } else {
      return WEEKDAYS[date.getDay()];
    }
  };
  
  export const getSearchTimeString = (
    anyday: boolean,
    today: boolean,
    tomorrow: boolean,
  ): string => {
    if (anyday) {
      return 'Any Day';
    }
    if (today && tomorrow) {
      return 'Today, Tomorrow';
    } else if (today) {
      return 'Today';
    } else if (tomorrow) {
      return 'Tomorrow';
    }
    return 'ERROR SEARCH TIME';
  };
  
  export const toFinnishDay = (date: Date): string => {
    return (
      date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
    );
  };

  export const toLocaleTime = (date: Date): string => {
    return date.toLocaleTimeString().slice(0, -3);
  }

  export const compareNotificationsByTime = (a: SepoNotificationWithId, b: SepoNotificationWithId): number => {
    return b.time.getTime() - a.time.getTime();
  }