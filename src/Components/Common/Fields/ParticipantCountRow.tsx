import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  USER_ICON_COLOR,
  FIELD_BG_COLOR,
  FIELD_BORDER_RADIUS,
  FIELD_HEIGHT,
  FIELD_BORDER_WIDTH,
  FIELD_BORDER_COLOR,
  MEDIUM_FIELD_HEIGHT,
} from '../CommonStyles';
import {TouchableOpacity} from 'react-native-gesture-handler';

const buttonSize: number = 45;

interface ParticipantCountRowProps {
  value: number;
  setMethod(value: number): void;
  label: string;
  backgroundColor?: string;
  maxValue?: number;
}

const ParticipantCountRow: React.FC<ParticipantCountRowProps> = (
  props: ParticipantCountRowProps,
) => {
  const bgColor: string = props.backgroundColor
    ? props.backgroundColor
    : FIELD_BG_COLOR;

  const canAdd: boolean = props.maxValue
    ? props.value < props.maxValue
    : props.value < 99;
  const canDecrease: boolean = props.value > 1;

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: bgColor,
        justifyContent: 'space-between',
        marginVertical: 2,
        paddingHorizontal: 16,
        borderRadius: FIELD_BORDER_RADIUS,
        height: MEDIUM_FIELD_HEIGHT,
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        // borderWidth: FIELD_BORDER_WIDTH,
        // borderColor: FIELD_BORDER_COLOR,
      }}>
      <View style={styles.spinnerContainer}>
        <View style={styles.item}>
          <Icon
            name="user"
            color={USER_ICON_COLOR}
            style={styles.icon}
            size={25}
          />
          <Text style={{fontSize: 25}}>{props.value}</Text>

          <View style={styles.item}>
            <Text style={styles.spinnerText}>{props.label}</Text>
          </View>
        </View>
      </View>

      <View style={styles.spinnerButtons}>
        <TouchableOpacity
          disabled={!canDecrease}
          style={
            canDecrease
              ? {...styles.spinnerButton, backgroundColor: '#e2c232'}
              : {...styles.spinnerButtonDisabled, backgroundColor: '#f3e4a9'}
          }
          onPress={() => {
            if (props.value > 1) {
              props.setMethod(props.value - 1);
            }
          }}>
          <Text
            style={
              canDecrease
                ? styles.spinnerButtonText
                : styles.spinnerButtonTextDisabled
            }>
            -
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled={!canAdd}
          style={
            canAdd
              ? {...styles.spinnerButton, backgroundColor: '#c2ec73'}
              : {...styles.spinnerButtonDisabled, backgroundColor: '#d3f29c'}
          }
          onPress={() => {
            if (canAdd) {
              props.setMethod(props.value + 1);
            }
          }}>
          <Text
            style={
              canAdd
                ? styles.spinnerButtonText
                : styles.spinnerButtonTextDisabled
            }>
            +
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ParticipantCountRow;

const styles = StyleSheet.create({
  spinnerContainer: {
    flexDirection: 'row',
  },
  spinnerText: {
    fontSize: 16,
    marginLeft: 10,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginRight: 4,
  },
  spinnerButtons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinnerButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
    borderWidth: 1.5,
    height: buttonSize,
    width: buttonSize,
    borderRadius: 25,
    marginLeft: 8,
  },
  spinnerButtonDisabled: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#808080',
    borderWidth: 1.5,
    height: buttonSize,
    width: buttonSize,
    borderRadius: 25,
    marginLeft: 8,
  },
  spinnerButtonText: {
    fontSize: 25,
  },
  spinnerButtonTextDisabled: {
    fontSize: 25,
    color: '#808080',
  },
});
