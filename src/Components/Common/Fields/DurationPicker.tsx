import React, {useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {DURATIONS} from '../CommonVariables';
import {ListItem, Overlay} from 'react-native-elements';
import {
  DARK_MODE_BG_LIGHT_COLOR,
  FIELD_BORDER_RADIUS,
  commonFormStyles,
  DarkModeStyle,
} from '../CommonStyles';
import TouchableScale from 'react-native-touchable-scale';
import {useDarkMode} from '../useDarkMode';
import Icon from 'react-native-vector-icons/FontAwesome';
const _ = require('lodash');

interface DurationPickerProps {
  duration: string;
  setDuration(sport: string): void;
}

const DurationPicker: React.FC<DurationPickerProps> = (
  props: DurationPickerProps,
) => {
  const darkMode = useDarkMode();

  const [overlayVisible, setOverlayVisible] = useState<boolean>(false);
  return (
    <View style={commonFormStyles.pickerWrapper}>
      <Overlay
        isVisible={overlayVisible}
        onBackdropPress={() => setOverlayVisible(false)}>
        <FlatList
          data={DURATIONS}
          keyExtractor={(item: string, index: number) => index.toString()}
          renderItem={({item}: {item: string}) => (
            <ListItem
              title={item}
              onPress={() => {
                props.setDuration(item);
                setOverlayVisible(false);
              }}
            />
          )}
        />
      </Overlay>
      <ListItem
        Component={TouchableScale}
        friction={90}
        tension={100}
        activeScale={0.95}
        containerStyle={styles({isDarkMode: darkMode}).listItemContainer}
        titleStyle={styles({isDarkMode: darkMode}).listItemTitle}
        title={props.duration}
        onPress={() => setOverlayVisible(true)}
        leftIcon={
          <Icon
            name="hourglass-1"
            size={29}
            style={styles({isDarkMode: darkMode}).pickerLeftIcon}
          />
        }
        chevron
      />
    </View>
  );
};

const styles = (props: DarkModeStyle) =>
  StyleSheet.create({
    listItemContainer: {
      backgroundColor: props.isDarkMode ? DARK_MODE_BG_LIGHT_COLOR : '#fff',
      borderRadius: FIELD_BORDER_RADIUS,
    },
    listItemTitle: {
      color: props.isDarkMode ? '#fff' : '#000',
      fontSize: 15,
    },
    pickerLeftIcon: {
      marginLeft: 4,
      marginRight: 2,
    },
  });

export default DurationPicker;
