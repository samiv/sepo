import React from 'react';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {View} from 'react-native';
import {
  commonFormStyles,
} from '../CommonStyles';

interface LocationInputProps {
  value: string;
  onChange(text: string): void;
}

const LocationInput = (props: LocationInputProps) => {
  return (
    <View style={commonFormStyles.pickerWrapper}>
      <Input
        maxLength={40}
        placeholder="Location"
        onChangeText={text => props.onChange(text)}
        value={props.value}
        leftIconContainerStyle={{
          marginRight: 20,
        }}
        leftIcon={<Icon name="map-marker" size={24} color="black" />}
      />
    </View>
  );
};

export default LocationInput;
