import React from 'react';
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  FIELD_BG_COLOR,
  FIELD_BORDER_RADIUS,
  BIG_FIELD_HEIGHT,
  FIELD_BORDER_WIDTH,
  FIELD_BORDER_COLOR,
  MEDIUM_FIELD_HEIGHT,
  STAR_ICON_COLOR,
} from '../CommonStyles';

interface RatePickerProps {
  value: number;
  setMethod(setValue: number): void;
}

const RatePicker: React.FC<RatePickerProps> = (props: RatePickerProps) => {
  return (
    <View style={styles.ratePickerContainer}>

      <TouchableOpacity onPress={() => props.setMethod(1)}>
        <View
          style={
            props.value === 1
              ? [styles.ratePickerImage, styles.ratePickerImageSelected]
              : styles.ratePickerImage
          }>
          <View style={styles.stars}>
            <Icon name="star" color={STAR_ICON_COLOR} size={28} />
          </View>
          <Text style={styles.text}>Beginner</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.setMethod(2)}>
        <View
          style={
            props.value === 2
              ? [styles.ratePickerImage, styles.ratePickerImageSelected]
              : styles.ratePickerImage
          }>
          <View style={styles.stars}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                width: '100%',
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={25} />
              <Icon name="star" color={STAR_ICON_COLOR} size={25} />
            </View>
          </View>
          <Text style={styles.text}>Average</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.setMethod(3)}>
        <View
          style={
            props.value === 3
              ? [styles.ratePickerImage, styles.ratePickerImageSelected]
              : styles.ratePickerImage
          }>
          <View style={styles.stars}>
            <View style={{position: 'absolute', top: 1}}>
              <Icon name="star" color={STAR_ICON_COLOR} size={18} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                width: '100%',
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={18} />
              <Icon name="star" color={STAR_ICON_COLOR} size={18} />
            </View>
          </View>
          <Text style={styles.text}>Advanced</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.setMethod(4)}>
        <View
          style={
            props.value === 4
              ? [styles.ratePickerImage, styles.ratePickerImageSelected]
              : styles.ratePickerImage
          }>
          <View style={styles.stars}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                width: '100%',
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={17} style={{marginRight: 4}}/>
              <Icon name="star" color={STAR_ICON_COLOR} size={17} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                width: '100%',
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={17} style={{marginRight: 4}}/>
              <Icon name="star" color={STAR_ICON_COLOR} size={17} />
            </View>
          </View>
          <Text style={styles.text}>Expert</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.setMethod(5)}>
        <View
          style={
            props.value === 5
              ? [styles.ratePickerImage, styles.ratePickerImageSelected]
              : styles.ratePickerImage
          }>
          <View style={styles.stars}>
            <View
              style={{
                position: 'absolute',
                bottom: 10,
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={15} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                width: '100%',
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={15} />
              <Icon name="star" color={STAR_ICON_COLOR} size={15} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                width: '100%',
              }}>
              <Icon name="star" color={STAR_ICON_COLOR} size={15} />
              <Icon name="star" color={STAR_ICON_COLOR} size={15} />
            </View>
          </View>
          <Text style={styles.text}>Pro</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default RatePicker;

const styles = StyleSheet.create({
  ratePickerContainer: {
    height: MEDIUM_FIELD_HEIGHT,
    flexDirection: 'row',
    backgroundColor: FIELD_BG_COLOR,
    justifyContent: 'space-between',
    paddingVertical: 4,
    paddingHorizontal: 6,
    borderRadius: FIELD_BORDER_RADIUS,
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // borderWidth: FIELD_BORDER_WIDTH,
    // borderColor: FIELD_BORDER_COLOR,
  },
  ratePickerImage: {
    width: 55,
    height: 55,
    borderRadius: 4,
  },
  ratePickerImageSelected: {
    borderColor: '#6a32e2',
    borderWidth: 2,
    borderRadius: 12,
  },
  stars: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 35,
  },
  text: {
    fontSize: 11,
    textAlign: 'center',
  },
});
