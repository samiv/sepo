import React, {useState} from 'react';
import {View, Image, StyleSheet, FlatList} from 'react-native';
import {SPORTS, getImagePath} from '../CommonVariables';
import {Sport} from '../CommonTypes';
import {ListItem, Overlay} from 'react-native-elements';
import {
  DARK_MODE_BG_LIGHT_COLOR,
  FIELD_BORDER_RADIUS,
  commonFormStyles,
  DarkModeStyle,
} from '../CommonStyles';
import {connect, useSelector} from 'react-redux';
import {ApplicationState} from '../../../App';
import TouchableScale from 'react-native-touchable-scale';
import {useDarkMode} from '../useDarkMode';
const _ = require('lodash');

interface SportPickerProps {
  sport: string;
  setSport(sport: string): void;
}

interface PropsFromState {
  favouriteSports: string[];
}

const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    favouriteSports: appState.favouriteSports,
  };
};

type AllProps = SportPickerProps & PropsFromState;

const SportPicker: React.FC<AllProps> = (props: AllProps) => {
  const darkMode = useDarkMode();
  const [overlayVisible, setOverlayVisible] = useState<boolean>(false);

  const favouriteSportsFromState: Sport[] = _.filter(SPORTS, (s: Sport) =>
    props.favouriteSports.includes(s.name),
  );
  return (
    <View style={commonFormStyles.pickerWrapper}>
      <Overlay
        isVisible={overlayVisible}
        onBackdropPress={() => setOverlayVisible(false)}>
        <FlatList
          data={favouriteSportsFromState}
          keyExtractor={(item: Sport, index: number) => index.toString()}
          renderItem={({item}: {item: Sport}) => (
            <ListItem
              title={_.startCase(item.name)}
              rightIcon={
                <Image
                  source={item.imageFileName}
                  style={{width: 30, height: 30}}
                />
              }
              onPress={() => {
                props.setSport(item.name);
                setOverlayVisible(false);
              }}
            />
          )}
        />
      </Overlay>
      <ListItem
        Component={TouchableScale}
        friction={90}
        tension={100}
        activeScale={0.95}
        containerStyle={styles({isDarkMode: darkMode}).listItemContainer}
        titleStyle={styles({isDarkMode: darkMode}).listItemTitle}
        title={_.startCase(props.sport)}
        onPress={() => setOverlayVisible(true)}
        leftIcon={
          <Image
            source={getImagePath(props.sport)}
            style={styles({isDarkMode: darkMode}).listItemLeftIconImage}
          />
        }
        chevron
      />
    </View>
  );
};

const styles = (props: DarkModeStyle) =>
  StyleSheet.create({
    listItemContainer: {
      backgroundColor: props.isDarkMode ? DARK_MODE_BG_LIGHT_COLOR : '#fff',
      borderRadius: FIELD_BORDER_RADIUS,
    },
    listItemTitle: {
      color: props.isDarkMode ? '#fff' : '#000',
      fontSize: 15,
    },
    listItemSubTitle: {
      color: props.isDarkMode ? '#fff' : '#00a710',
      fontSize: 11,
    },
    listItemLeftIconImage: {
      height: 29,
      width: 29,
    },
  });

export default connect(mapStateToProps)(SportPicker);
