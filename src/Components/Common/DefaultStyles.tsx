
export const DefaultButtonTitleStyle = {
    marginLeft: 5,
    fontSize: 20,
  };
  
  export const DefaultButtonStyle = {
    width: 120,
    padding: 13,
    borderRadius: 20,
  };
  
  export const ButtonTitleStyle = {
    marginLeft: 5,
    fontSize: 20,
  };