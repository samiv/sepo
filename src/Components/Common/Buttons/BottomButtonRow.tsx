import React from 'react';
import {View, StyleSheet} from 'react-native';

interface BottomButtonRowProps {
  children: React.ReactNode;
  marginBottom?: number;
  justifyContent?: string;
  marginRight?: number;
}

interface ButtonRowStylesProps {
  marginBottom: number;
  justifyContent: JustifyContentType;
  marginRight: number;
}

type JustifyContentType = "space-around" | "flex-start" | "flex-end" | "center" | "space-between" | "space-evenly" | undefined;

const BottomButtonRow: React.FC<BottomButtonRowProps> = (
  props: BottomButtonRowProps,
) => {
  const marginBot: number = props.marginBottom ? props.marginBottom : 16;
  const justifyContent: JustifyContentType = props.justifyContent ? props.justifyContent : "space-around";
  const marginRight: number = props.marginRight ? props.marginRight : 0;

  return (
    <View style={styles({marginBottom: marginBot, justifyContent: justifyContent, marginRight: marginRight}).buttonRow}>
      {props.children}
    </View>
  );
};

export default BottomButtonRow;

const styles = (props: ButtonRowStylesProps) =>
  StyleSheet.create({
    buttonRow: {
      flexDirection: 'row',
      justifyContent: props.justifyContent,
      marginBottom: props.marginBottom,
      marginRight: props.marginRight,
    },
  });  
