import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

interface ButtonIconProps {
  name: string;
  color: string;
}

const ButtonIcon: React.FC<ButtonIconProps> = (props: ButtonIconProps) => {
  return <Icon name={props.name} size={30} color={props.color} />;
};

export default ButtonIcon;
