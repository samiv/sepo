import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text} from 'react-native-elements';
import {NETURAL_BUTTON_COLOR} from '../CommonStyles';

interface CircleButtonProps {
  onPress(): void;
  iconName: string;
  backgroundColor?: string;
  width?: number;
  bottomText?: string;
  iconSize?: number;
  color?: string;
  // borderColor?: string;
}

const CircleButton: React.FC<CircleButtonProps> = (
  props: CircleButtonProps,
) => {
  const circleWitdh: number = props.width ? props.width : 70;
  const background: string = props.backgroundColor
    ? props.backgroundColor
    : '#fff';
  const color: string = props.color ? props.color : '#01a699';
  const iconSize = props.iconSize ? props.iconSize : 23;
  // const borderColor = props.borderColor ? props.borderColor : '#fff';
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        width: circleWitdh + 20,
        height: circleWitdh + 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: circleWitdh,
          height: circleWitdh,
          backgroundColor: background,
          borderRadius: circleWitdh / 2,
          // borderColor: borderColor,
        }}>
        <Icon name={props.iconName} size={iconSize} color={color} />
        {props.bottomText && (
          <Text style={{color: color}}>{props.bottomText}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default CircleButton;
