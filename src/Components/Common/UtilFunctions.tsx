import {SepoNotification} from './CommonTypes';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {APP_MAIN_COLOR, HEADER_TEXT_COLOR} from './CommonStyles';
import {Divider} from 'react-native-elements';
import {toFinnishDay} from './DateUtils';

const _ = require('lodash');

export const getSepoNotificationTitle = (item: SepoNotification): string => {
  return _.upperFirst(item.topic) + ', ' + toFinnishDay(item.time);
};

export const getAllRates = (lower: number, upper: number): number[] => {
  let retVal: number[] = [];
  for (let i = lower; i <= upper; i++) {
    retVal.push(i);
  }
  return retVal;
};

export function getHeaderStyles(icons: boolean, title?: string) {
  const navigation = useNavigation();
  const titleString: string = title ? title : 'SePo';
  const textAlignString: string = icons ? 'center' : 'left';

  if (!icons) {
    return {
      title: titleString,
      headerTitleStyle: {
        textAlign: textAlignString,
        flex: 1,
        fontWeight: 'bold',
      },
      headerStyle: {
        backgroundColor: APP_MAIN_COLOR,
      },
      headerTintColor: HEADER_TEXT_COLOR,
    };
  }

  return {
    title: titleString,
    headerTitleStyle: {
      textAlign: 'center',
      flex: 1,
      fontWeight: 'bold',
    },
    headerStyle: {
      backgroundColor: APP_MAIN_COLOR,
    },
    headerTintColor: HEADER_TEXT_COLOR,
    // headerRight: () => (
    //   <TouchableOpacity
    //     style={{marginRight: 12}}
    //     onPress={() => navigation.navigate('Settings')}>
    //     <Icon name="cog" size={27} color="#fff" />
    //   </TouchableOpacity>
    // ),
    // headerLeft: () => (
    //   <TouchableOpacity
    //     style={{marginLeft: 12}}
    //     onPress={() => navigation.navigate('Notifications')}>
    //     <Icon name="home" size={27} color="#fff" />
    //   </TouchableOpacity>
    // ),
  };
}

export const getRateString = (rates: number[]): string => {
  if (rates.length === 1) {
    return rates[0].toString();
  } else {
    return rates[0].toString() + ' - ' + rates[rates.length - 1].toString();
  }
};

export const DefaultDivider = () => {
  return <Divider style={{height: 1, backgroundColor: 'black'}} />;
};
