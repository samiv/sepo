import {Sport} from './CommonTypes';

const _ = require('lodash');

export const SPORTS: Sport[] = [
  {name: 'tennis', imageFileName: require('../../Assets/Images/racket.png')},
  {name: 'padel', imageFileName: require('../../Assets/Images/padel.png')},
  {name: 'beach volley', imageFileName: require('../../Assets/Images/beach-volleyball.png')},
];

export const getSportNames = (): string[] => {
  let retVal: string[] = [];
  for(let i = 0; i < SPORTS.length; ++i) {
    retVal.push(SPORTS[i].name);
  }
  return retVal;
} 

export const getImagePath = (topic: string) => {
  const retVal: Sport | undefined = SPORTS.find((s: Sport) => s.name === topic);
  return retVal ? retVal.imageFileName : require('../../Assets/Images/racket.png');
};

export const DURATIONS: string[] = [
  '15min',
  '30min',
  '45min',
  '1h',
  '1h 15min',
  '1h 30min',
  '1h 45min',
  '2h',
  '2h 30min',
  '3h',
  '> 3h',
];

export const RATES: number[] = [1, 2, 3, 4, 5];

export enum Rate {
  BEGINNER,
  AVERAGE,
  ADVANCED,
  EXPERT,
  PROFESSIONAL,
}

export const DEFAULT_TOPIC: string = "Topic";
