import {
  SepoNotification,
  SepoNotificationWithId,
  WEEKDAYS,
} from './CommonTypes';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {APP_MAIN_COLOR, HEADER_TEXT_COLOR} from './CommonStyles';
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {Divider} from 'react-native-elements';

const _ = require('lodash');

export function getUser() {
  const user: FirebaseAuthTypes.User | null = auth().currentUser;
  return user ? user : null;
}

export function getUserId(): string {
  const user: FirebaseAuthTypes.User | null = auth().currentUser;
  return user ? user.uid : 'ERROR ID FOR USER';
}

export function getUserDisplayame(): string {
  const user: FirebaseAuthTypes.User | null = auth().currentUser;
  const displayName: string | null = user ? user.displayName : null;
  return displayName ? displayName : 'ERROR USERNAME FOR USER';
}

export function getUserEmail(): string {
  const user: FirebaseAuthTypes.User | null = auth().currentUser;
  const email: string | null = user ? user.email : null;
  return email ? email : 'ERROR EMAIL FOR USER';
}

export async function searchEvents(
  topic: string,
  rate: number,
  attendees: number,
  anyDay: boolean,
  today: boolean,
  tomorrow: boolean,
  setResults: any,
) {
  let arr: SepoNotificationWithId[] = [];
  const query = await firestore()
    .collection('notifications')
    .where('topic', '==', topic)
    .where('rates', 'array-contains', rate)
    .get();

  query.forEach(docSnapshot => {
    if (showOnSearch(docSnapshot, getUserId(), attendees)) {
      if (
        anyDay ||
        validateToday(today, docSnapshot.data().time.toDate()) ||
        validateTomorrow(tomorrow, docSnapshot.data().time.toDate())
      ) {
        addNotificationDocToArray(docSnapshot, arr);
      }
    }
  });

  setResults(arr);
}
const validateToday = (isTodaySearch: boolean, date: Date): boolean => {
  if (!isTodaySearch) {
    return false;
  }
  const now = new Date();
  return (
    date.getDate() === now.getDate() &&
    date.getMonth() === now.getMonth() &&
    date.getFullYear() === now.getFullYear()
  );
};

const validateTomorrow = (isTomorrowSearch: boolean, date: Date): boolean => {
  if (!isTomorrowSearch) {
    return false;
  }

  const now = new Date();
  let tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

  return (
    date.getDate() === tomorrow.getDate() &&
    date.getMonth() === tomorrow.getMonth() &&
    date.getFullYear() === tomorrow.getFullYear()
  );
};

export const addNotificationDocToArray = (
  doc: any,
  arr: SepoNotificationWithId[],
) => {
  arr.push({
    id: doc.id,
    topic: doc.data().topic,
    rates: doc.data().rates,
    time: doc.data().time.toDate(),
    location: doc.data().location,
    participants: doc.data().participants,
    minParticipants: doc.data().minParticipants,
    maxParticipants: doc.data().maxParticipants,
    initParticipants: doc.data().initParticipants,
    duration: doc.data().duration,
    owner: doc.data().owner,
  });
};

const showOnSearch = (doc: any, userId: string, attendees: number): boolean => {
  const parts: string[] = doc.data().participants;
  if (doc.data().owner === userId) {
    return false;
  }
  if (parts.includes(userId)) {
    return false;
  }
  if (doc.data().maxParticipants - parts.length < attendees) {
    return false;
  }

  return true;
};
