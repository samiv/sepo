import AsyncStorage from '@react-native-community/async-storage';
import {SavedSearch} from './CommonTypes';

const shortid = require('shortid');

export const ASYNC_STORAGE_SAVED_SEARCHES_KEY = 'SAVED_SEARCHES';
export const ASYNC_STORAGE_FAVOURITE_SPORT_KEY = 'FAVOURITE_SPORT';

export const storeStringValue = async (value: string, key: string) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.error(e);
  }
};

export const getDataFromAsyncStorage = async (key: string) => {
  try {
    const res = await AsyncStorage.getItem(key);
    // console.warn(res);
    return res;
  } catch (e) {
    // error reading value
  }
};

const idInUse = (id: string, searches: any): boolean => {
  for (let i: number = 0; i > searches.length; ++i) {
    if (searches[i].id === id) {
      return true;
    }
  }
  return false;
};

export const storeSearchesToAsyncStorage = async (value: SavedSearch) => {
  const newSearches: SavedSearch[] = [value];
  try {
    const current = await AsyncStorage.getItem(
      ASYNC_STORAGE_SAVED_SEARCHES_KEY,
    );
    const currentObject = current ? JSON.parse(current) : null;
    if (currentObject != null) {
      for (var i = 0; i < currentObject.length; i++) {
        newSearches.push(currentObject[i]);
      }
    }

    await AsyncStorage.setItem(
      ASYNC_STORAGE_SAVED_SEARCHES_KEY,
      JSON.stringify(newSearches),
    );
  } catch (e) {
    console.error(e);
  }
};

export const removeSearchValue = async (idToRemove: string) => {
  const updatedSearches: SavedSearch[] = [];
  try {
    const current = await AsyncStorage.getItem(
      ASYNC_STORAGE_SAVED_SEARCHES_KEY,
    );
    const currentObject = current ? JSON.parse(current) : null;
    for (var i = 0; i < currentObject.length; i++) {
      if (currentObject[i].id !== idToRemove) {
        updatedSearches.push(currentObject[i]);
      }
    }
    await AsyncStorage.setItem(
      ASYNC_STORAGE_SAVED_SEARCHES_KEY,
      JSON.stringify(updatedSearches),
    );
  } catch (e) {
    console.error(e);
  }
};

export const clearAll = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    // clear error
  }

  console.log(' Clearing Done.');
};

export const generateSearchId = async () => {
  const jsonValue = await AsyncStorage.getItem(
    ASYNC_STORAGE_SAVED_SEARCHES_KEY,
  );
  const saved = jsonValue != null ? JSON.parse(jsonValue) : null;
  let newId: string;

  do {
    newId = shortid.generate();
  } while (idInUse(newId, saved));

  return newId;
};
