import {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {ApplicationState} from '../../App';

export function useDarkMode() {
  const isDarkMode = useSelector(
    (state: ApplicationState) => state.appState.isDarkMode,
  );
  return isDarkMode;
}
