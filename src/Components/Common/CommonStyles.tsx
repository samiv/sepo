import {StyleSheet} from 'react-native';

export const DEFAULT_BG_COLOR: string = '#e5f6c3';
export const SCREEN_DEFAULT_PADDING: number = 24;

// Navitgation components --------------------------------
export const APP_MAIN_COLOR: string = '#AAE232'; // mm. navigation header
export const APP_SECONDARY_COLOR: string = '#92b91e'; // mm. statusbar
export const TOP_TABS_BG_COLOR: string = '#d3f29c';

export const HEADER_TEXT_COLOR: string = '#fff';
export const TOP_TABS_ACTIVE_TINT_COLOR: string = '#6f7b00';
export const TOP_TABS_TEXT_COLOR: string = '#85a211';

export const BOTTOM_TAB_ACTIVE_BG: string = '#f4fce7';
// ------------------------------------------------------

// Field colors -----------------------------------------
export const FIELD_BG_COLOR: string = '#fff';
export const FIELD_BORDER_WIDTH: number = 1;
export const FIELD_BORDER_COLOR: string = '#85a211';
export const FIELD_HEIGHT: number = 50;
export const MEDIUM_FIELD_HEIGHT: number = 63;
export const BIG_FIELD_HEIGHT: number = 69;
export const PICKER_MARGIN_LEFT: number = 4;
export const FIELD_BORDER_RADIUS: number = 15;
export const DEFAULT_FIELD_VERTICAL_MARGIN: number = 8;

export const commonFormStyles = StyleSheet.create({
  pickerWrapper: {
    justifyContent: 'center',
    backgroundColor: FIELD_BG_COLOR,
    borderRadius: FIELD_BORDER_RADIUS,
    marginVertical: DEFAULT_FIELD_VERTICAL_MARGIN,
    // borderWidth: FIELD_BORDER_WIDTH,
    // borderColor: FIELD_BORDER_COLOR,
    height: MEDIUM_FIELD_HEIGHT,
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  dateTimeRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: DEFAULT_FIELD_VERTICAL_MARGIN,
    padding: 8,
    backgroundColor: FIELD_BG_COLOR,
    // borderColor: FIELD_BORDER_COLOR,
    borderRadius: FIELD_BORDER_RADIUS,
    // borderWidth: FIELD_BORDER_WIDTH,
    height: MEDIUM_FIELD_HEIGHT,
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
});
// ------------------------------------------------------

export const SEARCH_COLOR: string = '#b5f3a3';

export const TODAY_START_COLOR: string = '#f0a967';
export const TODAY_MEDIUM_COLOR: string = '#edb580';
export const TODAY_END_COLOR: string = '#f2c69d';

export const ACCEPT_COLOR: string = '#3fd129';
export const NETURAL_BUTTON_COLOR: string = '#5e2ddc';
export const REMOVE_COLOR: string = '#dd5800';
export const LOCK_COLOR: string = '#e2c232';

export const USER_ICON_COLOR: string = '#050cc4';
export const STAR_ICON_COLOR: string = '#deb400';

export const DEFAULT_ICON_SIZE: number = 30;
export const DEFAULT_BUTTON_WIDTH: number = 65;

// Dark Mode
export const DARK_MODE_BG_COLOR: string = '#15261d';
export const DARK_MODE_BG_LIGHT_COLOR: string = '#0f3814';

// NotificationList

export const commonListStyles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 20,
    backgroundColor: DEFAULT_BG_COLOR,
  },
  list: {
    paddingHorizontal: SCREEN_DEFAULT_PADDING,
  },
  today: {
    marginBottom: 16, // total 40 because group has always marginBottom: 8
  },
  todayText: {
    marginLeft: 8,
    fontSize: 17,
    fontWeight: '700',
  },
  group: {
    borderRadius: FIELD_BORDER_RADIUS,
    marginBottom: 8, // this is needed to show shadow correctly also to last item

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  todayGroup: {
    marginTop: 4,
  },
  restGroup: {
    marginTop: 12,
  },
  notificationListItemContainer: {
    marginVertical: 4,
  },
  restNotificationsGroup: {
    marginTop: 16,
  },
});

export interface DarkModeStyle {
  isDarkMode: boolean;
}

export const settingsStyles = (props: DarkModeStyle) =>
  StyleSheet.create({
    titleIcon: {
      marginRight: 20,
      padding: 15,
    },
    listItemContainer: {
      backgroundColor: props.isDarkMode ? DARK_MODE_BG_LIGHT_COLOR : '#fff',
    },
    listItemTitle: {
      color: props.isDarkMode ? '#fff' : '#000',
      fontSize: 15,
    },
    listItemSubTitle: {
      color: props.isDarkMode ? '#fff' : '#00a710',
      fontSize: 11,
    },
    listItemLeftIconImage: {
      height: 29,
      width: 29,
    },
    usernameInput: {
      marginBottom: 24,
      marginTop: 4,
      backgroundColor: FIELD_BG_COLOR,
    },
    usernameButton: {
      width: 100,
    },
    buttonRow: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    logoutButtonContainer: {
      paddingVertical: 64,
    },
    logoutButton: {
      alignSelf: 'center',
      width: 150,
    },
    logoutButtonIcon: {
      marginLeft: 8,
    },
  });
