import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {APP_MAIN_COLOR} from './CommonStyles';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';

interface CustomHeaderProps {
  title: string;
  onBackPress?(): void;
}

const CustomHeader: React.FC<CustomHeaderProps> = (
  props: CustomHeaderProps,
) => {
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.titleRow}>
        <Text style={styles.title}>{props.title}</Text>
      </View>
      <Icon
        name="arrow-left"
        size={25}
        style={styles.backButtonIcon}
        color="#fff"
        onPress={
          props.onBackPress ? props.onBackPress : () => navigation.goBack()
        }
      />
    </>
  );
};

const styles = StyleSheet.create({
  titleRow: {
    backgroundColor: APP_MAIN_COLOR,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backButtonIcon: {
    padding: 20,
    position: 'absolute',
    left: -5,
    top: -5,
  },
  title: {
    color: '#fff',
    fontSize: 22,
    fontWeight: '700',
  },
});

export default CustomHeader;
