import {ImageSourcePropType} from 'react-native';

export interface SepoNotification {
  topic: string;
  time: Date;
  duration: string;
  rates: number[];
  location: string;
  initParticipants: number;
  minParticipants: number;
  maxParticipants: number;
  participants: Array<string>;
  owner: string;
}

export interface SepoNotificationWithId extends SepoNotification {
  id: string;
}

export interface SavedSearch {
  id: string;
  topic: string;
  rate: number;
  attendees: number;
  anyDay: boolean;
  today: boolean;
  tomorrow: boolean;
}

export function getEmptySavedSearch(): SavedSearch {
  return {
    id: '',
    topic: '',
    rate: 1,
    attendees: 0,
    anyDay: false,
    today: false,
    tomorrow: false,
  };
}

export enum Category {
  SPORT,
  BOARDGAMES,
  DINNER,
  BABIES,
}

export interface Sport {
  name: string;
  imageFileName: ImageSourcePropType;
}

export const WEEKDAYS: string[] = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];
