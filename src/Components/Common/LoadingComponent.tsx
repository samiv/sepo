import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {Text} from 'react-native-elements';
import { APP_MAIN_COLOR } from './CommonStyles';

interface AllProps {
  text: string;
}

const LoadingComponent: React.FC<AllProps> = (props: AllProps) => {
  return (
    <View style={styles.loadingContent}>
      <ActivityIndicator size="large" color={APP_MAIN_COLOR}/>
      <Text style={styles.textStyle}>{props.text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
    loadingContent: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        fontSize: 20,
        color: APP_MAIN_COLOR,
    },
});

export default LoadingComponent;
