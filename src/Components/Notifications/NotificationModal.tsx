import React, {useEffect, useState} from 'react';
import {Text} from 'react-native-elements';
import {
  NavigationScreenProp,
  NavigationState,
  NavigationParams,
} from 'react-navigation';
import {BackHandler, StyleSheet, View} from 'react-native';
import {ApplicationState} from '../../App';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  APP_MAIN_COLOR,
  STAR_ICON_COLOR,
  USER_ICON_COLOR,
  DEFAULT_ICON_SIZE,
  TODAY_START_COLOR,
  TODAY_END_COLOR,
  TODAY_MEDIUM_COLOR,
} from '../Common/CommonStyles';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import {getRateString, DefaultDivider} from '../Common/UtilFunctions';
import {
  getDayString,
  toFinnishDay,
  isToday,
  toLocaleTime,
} from '../Common/DateUtils';
import LinearGradient from 'react-native-linear-gradient';
import NotificationModalActionButtons from './NotificationModalActionButtons';
import ParticipantCountRow from '../Common/Fields/ParticipantCountRow';
import {useNavigation} from '@react-navigation/native';
var _ = require('lodash');

interface NotificationModalProps {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface PropsFromState {
  openedNotification: SepoNotificationWithId;
  acceptable: boolean;
}

type AllProps = NotificationModalProps & PropsFromState;

const mapStateToProps = ({notModal}: ApplicationState) => {
  return {
    openedNotification: notModal.openedNotification,
    acceptable: notModal.accectable,
  };
};

const NotificationModal: React.FC<AllProps> = (props: AllProps) => {
  const [attendeeCount, setAttendeeCount] = useState<number>(1);
  const navigation = useNavigation();

  const gradientMain: string = isToday(props.openedNotification.time)
    ? TODAY_START_COLOR
    : APP_MAIN_COLOR;
  const gradientMid: string = isToday(props.openedNotification.time)
    ? TODAY_MEDIUM_COLOR
    : '#c9e86f';
  const gradientEnd: string = isToday(props.openedNotification.time)
    ? TODAY_END_COLOR
    : '#e6faac';

  useEffect(() => {
    const onBackPress = () => {
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      onBackPress,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <LinearGradient
      colors={[gradientMain, gradientMid, gradientEnd, 'transparent']}
      locations={[0.1, 0.25, 0.8, 0.99]}
      style={{flex: 1}}>
      <Icon
        name="arrow-left"
        size={DEFAULT_ICON_SIZE}
        style={styles.backButton}
        onPress={() => navigation.goBack()}
      />
      <View style={styles.container}>
        <Text style={styles.modalTitle}>
          {_.upperFirst(props.openedNotification.topic)}
        </Text>

        <View style={styles.modalSubTitleContainer}>
          <View style={styles.modalRow}>
            <Icon
              name="calendar"
              size={DEFAULT_ICON_SIZE}
              style={styles.iconRowStart}
            />
            <Text style={styles.modalText}>
              {toFinnishDay(props.openedNotification.time) +
                ' (' +
                getDayString(props.openedNotification.time) +
                ')'}
            </Text>
          </View>
          <DefaultDivider />
          <View style={styles.modalRow}>
            <Icon
              name="clock-o"
              size={DEFAULT_ICON_SIZE}
              style={styles.iconRowStart}
            />
            <Text style={styles.modalText}>
              {toLocaleTime(props.openedNotification.time) +
                ' (' +
                props.openedNotification.duration +
                ')'}
            </Text>
          </View>
          <DefaultDivider />
          <View style={styles.modalRow}>
            <Icon
              name="map-marker"
              size={DEFAULT_ICON_SIZE}
              style={styles.iconRowStart}
            />
            <Text style={styles.modalText}>
              {_.upperFirst(props.openedNotification.location)}
            </Text>
          </View>
          <DefaultDivider />

          <View style={styles.modalRow}>
            <Icon
              name="star"
              size={DEFAULT_ICON_SIZE}
              style={styles.iconRowStart}
              color={STAR_ICON_COLOR}
            />
            <Text style={styles.modalText}>
              {getRateString(props.openedNotification.rates)}
            </Text>
          </View>
          <DefaultDivider />

          <View style={styles.modalRow}>
            <Icon
              name="user"
              size={DEFAULT_ICON_SIZE}
              style={styles.iconRowStart}
              color={USER_ICON_COLOR}
            />
            <Text style={styles.modalText}>
              {props.openedNotification.participants.length + ''}
            </Text>

            <Text style={styles.modalText}>
              {' / ' + props.openedNotification.maxParticipants + ' '}
            </Text>

            {props.openedNotification.maxParticipants >
              props.openedNotification.minParticipants && (
              <Text style={{...styles.modalText}}>
                {' (Min ' + props.openedNotification.minParticipants + ')'}
              </Text>
            )}
          </View>
          <DefaultDivider />
        </View>
        {props.acceptable && (
          <View style={styles.attendeeCountRow}>
            <ParticipantCountRow
              value={attendeeCount}
              setMethod={setAttendeeCount}
              label="Attendee(s)"
              maxValue={
                props.openedNotification.maxParticipants -
                props.openedNotification.participants.length
              }
            />
          </View>
        )}
      </View>

      <NotificationModalActionButtons
        acceptable={props.acceptable}
        openedNotification={props.openedNotification}
        attendeeCount={attendeeCount}
      />
    </LinearGradient>
  );
};

export default connect(mapStateToProps)(NotificationModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingHorizontal: 45,
  },
  backButton: {
    position: 'absolute',
    left: 4,
    top: 8,
    padding: 16,
  },
  modalTitle: {
    fontSize: 40,
    textAlign: 'center',
  },
  modalSubTitleContainer: {
    marginVertical: 24,
  },
  modalText: {
    fontSize: 25,
  },
  modalRow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13,
  },
  iconRowStart: {
    width: 30,
    textAlign: 'center',
    marginRight: 12,
  },
  attendeeCountRow: {
    paddingVertical: 13,
  },
});
