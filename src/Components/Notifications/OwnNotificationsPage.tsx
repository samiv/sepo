import React, {useEffect, useState} from 'react';
import {View, ScrollView, BackHandler} from 'react-native';
import {Text} from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import {useNavigation} from '@react-navigation/native';
import {connect} from 'react-redux';
import NotificationListItem from './NotificationListItem';
import {
  setModalNotificationAcceptable,
  setModalNotification,
} from '../store/NotificationModal/notificationModalActions';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import {addNotificationDocToArray, getUserId} from '../Common/FirebaseUtils';
import LoadingComponent from '../Common/LoadingComponent';
import {isToday, compareNotificationsByTime} from '../Common/DateUtils';
import {commonListStyles} from '../Common/CommonStyles';
import NoRequestsPage from './NoRequestsPage';

interface OwnNotificationsProps {}

interface PropsFromState {}

interface DispatchProps {
  setModalNotificationAcceptable: typeof setModalNotificationAcceptable;
  setModalNotification: typeof setModalNotification;
}

const mapDispatchToProps: DispatchProps = {
  setModalNotificationAcceptable: setModalNotificationAcceptable,
  setModalNotification: setModalNotification,
};

type AllProps = OwnNotificationsProps & PropsFromState & DispatchProps;

const OwnNotificationsPage: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  const [ownNotsToday, setOwnNotsToday] = useState<
    Array<SepoNotificationWithId>
  >([]);
  const [ownNotsRest, setOwnNotsRest] = useState<Array<SepoNotificationWithId>>(
    [],
  );
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const userId = getUserId();

  useEffect(() => {
    const ownNotificationListener = firestore()
      .collection('notifications')
      .where('owner', '==', userId)
      .onSnapshot(querySnapshot => {
        const today: Array<SepoNotificationWithId> = [];
        const rest: Array<SepoNotificationWithId> = [];
        querySnapshot.docs.map(doc => {
          if (doc.data().maxParticipants > doc.data().minParticipants) {
            if (isToday(doc.data().time.toDate())) {
              addNotificationDocToArray(doc, today);
            } else {
              addNotificationDocToArray(doc, rest);
            }
          }
        });
        setOwnNotsToday(today.sort(compareNotificationsByTime));
        setOwnNotsRest(rest.sort(compareNotificationsByTime));
        setIsLoading(false);
      });

    return () => ownNotificationListener();
  }, []);

  if (isLoading) {
    return <LoadingComponent text="Loading..." />;
  }

  if (ownNotsToday.length === 0 && ownNotsRest.length === 0) {
    return <NoRequestsPage firstTextRow="No Sent" secondTextRow="Request" />;
  }

  return (
    <View style={commonListStyles.container}>
      <ScrollView contentContainerStyle={commonListStyles.list}>
        {ownNotsToday.length > 0 && (
          <View style={commonListStyles.today}>
            <Text style={commonListStyles.todayText}>Today</Text>
            {ownNotsToday.map((n: SepoNotificationWithId, index: number) => {
              return (
                <View
                  style={commonListStyles.notificationListItemContainer}
                  key={index.toString()}>
                  <NotificationListItem
                    item={n}
                    index={index}
                    itemCount={ownNotsToday.length}
                    today={true}
                    onPress={() => {
                      props.setModalNotificationAcceptable(false);
                      props.setModalNotification(n);
                      navigation.navigate('NotificationModal');
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}

        {ownNotsRest.length > 0 && (
          <View style={commonListStyles.restNotificationsGroup}>
            {ownNotsRest.map((n: SepoNotificationWithId, index: number) => {
              return (
                <View
                  style={commonListStyles.notificationListItemContainer}
                  key={index.toString()}>
                  <NotificationListItem
                    item={n}
                    index={index}
                    itemCount={ownNotsRest.length}
                    today={false}
                    onPress={() => {
                      props.setModalNotificationAcceptable(false);
                      props.setModalNotification(n);
                      navigation.navigate('NotificationModal');
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(OwnNotificationsPage);
