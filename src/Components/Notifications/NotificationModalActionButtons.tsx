import React from 'react';
import {StyleSheet, View, Alert} from 'react-native';
import {
  ACCEPT_COLOR,
  REMOVE_COLOR,
  LOCK_COLOR,
} from '../Common/CommonStyles';
import firestore from '@react-native-firebase/firestore';
import {useNavigation} from '@react-navigation/native';
import {showMessage} from 'react-native-flash-message';
import CircleButton from '../Common/Buttons/CircleButton';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import { getUserId } from '../Common/FirebaseUtils';
var _ = require('lodash');

async function acceptNotification(id: string, attendeeCount: number) {
  let isFull: boolean = false;
  let exists: boolean = true;
  let participants: string[] = [];

  await firestore()
    .collection('notifications')
    .doc(id)
    .get()
    .then(documentSnapshot => {
      if (documentSnapshot.exists) {
        const max: number = documentSnapshot.data().maxParticipants;
        participants = documentSnapshot.data().participants;
        isFull = participants.length + attendeeCount > max;
        for(let i = 0; i < attendeeCount; ++i) {
          participants.push(getUserId());
        }
      } else {
        exists = false;
      }
    });

  // TODO: NÄMÄ TÄYTYY TESTATA HUOLELLA LISÄÄMÄLLÄ FIREBASEEN PARTICIPANT KUN MODAALI ON AUKI TAI POISTAMALLA KOKO HÖSKÄ
  if (!exists) {
    showErrorMessage('Failed to Accept. Event does not exists!');
  } else if (isFull) {
    showErrorMessage('Failed to Accept. Not enough free spots!');
  } else {
    await firestore()
      .collection('notifications')
      .doc(id)
      .update({
        participants: participants,
      })
      .then(() => {
        console.log('Participants updated!');
      });
  }
}

async function lockNotification(notId: string) {
  let participantsCount: string[] = [];
  await firestore()
    .collection('notifications')
    .doc(notId)
    .get()
    .then(documentSnapshot => {
      participantsCount = documentSnapshot.data().participants.length;
    });

  await firestore()
    .collection('notifications')
    .doc(notId)
    .update({
      maxParticipants: participantsCount,
    })
    .then(() => {
      console.log('Participants updated!');
    });
}

function showErrorMessage(message: string) {
  showMessage({
    message: 'Failed to Accept. Event is already full',
    type: 'danger',
    duration: 6000,
    titleStyle: {
      fontSize: 16,
    },
    icon: 'danger',
  });
}

async function withdrawFromNotification(notId: string, userId: string) {
  let newParticipants: string[] = [];
  await firestore()
    .collection('notifications')
    .doc(notId)
    .get()
    .then(documentSnapshot => {
      newParticipants = documentSnapshot.data().participants;
    });

  _.remove(newParticipants, (p: string) => {
    return p === getUserId();
  });

  await firestore()
    .collection('notifications')
    .doc(notId)
    .update({
      participants: newParticipants,
    })
    .then(() => {
      console.warn('Withdraw!');
    });
}

async function removeNotification(id: string) {
  await firestore()
    .collection('notifications')
    .doc(id)
    .delete()
    .then(() => {
      console.warn('Notification deleted!');
    });
}

interface NotificationModalActionButtonsProps {
  acceptable: boolean;
  openedNotification: SepoNotificationWithId;
  attendeeCount: number;
}

const cirleButtonWidth: number = 80;

const NotificationModalActionButtons: React.FC<
  NotificationModalActionButtonsProps
> = (props: NotificationModalActionButtonsProps) => {
  const navigation = useNavigation();

  return (
    <View style={styles.modalButtons}>
      {/* <CircleButton
        iconName="arrow-left"
        onPress={() => navigation.goBack()}
        color="#fff"
        backgroundColor={NETURAL_BUTTON_COLOR}
        bottomText="Back"
        width={cirleButtonWidth}
      /> */}
      {props.acceptable && (
        <CircleButton
          iconName="check"
          onPress={() => {
            acceptNotification(props.openedNotification.id, props.attendeeCount);
            navigation.navigate('Custom');
          }}
          color="#fff"
          backgroundColor={ACCEPT_COLOR}
          bottomText="Accept"
          width={cirleButtonWidth}
        />
      )}
      {props.openedNotification.owner === getUserId() && (
        <>
          {props.openedNotification.participants.length <
            props.openedNotification.minParticipants && (
            <CircleButton
              iconName="trash"
              onPress={() => {
                Alert.alert(
                  'Remove event?',
                  'Other attendees are informed and might not be happy :(',
                  [
                    {
                      text: 'Cancel',
                      style: 'cancel',
                    },
                    {
                      text: 'OK',
                      onPress: () => {
                        removeNotification(props.openedNotification.id);
                        navigation.goBack();
                      },
                    },
                  ],
                  {cancelable: false},
                );
              }}
              color="#fff"
              backgroundColor={REMOVE_COLOR}
              bottomText="Remove"
              width={cirleButtonWidth}
            />
          )}
          {props.openedNotification.participants.length >=
            props.openedNotification.minParticipants && (
            <CircleButton
              iconName="lock"
              onPress={() => {
                Alert.alert(
                  'Lock event?',
                  'Users cannot join this event after lock',
                  [
                    {
                      text: 'Cancel',
                      style: 'cancel',
                    },
                    {
                      text: 'OK',
                      onPress: () => {
                        lockNotification(props.openedNotification.id);
                        navigation.goBack();
                      },
                    },
                  ],
                  {cancelable: false},
                );
              }}
              color="#fff"
              backgroundColor={LOCK_COLOR}
              bottomText="Lock"
              width={cirleButtonWidth}
            />
          )}
        </>
      )}
      {!props.acceptable && props.openedNotification.owner !== getUserId() && (
        <CircleButton
          iconName="sign-out"
          onPress={() => {
            Alert.alert(
              'Withdraw from this event?',
              '',
              [
                {
                  text: 'Cancel',
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    withdrawFromNotification(
                      props.openedNotification.id,
                      getUserId(),
                    );
                    navigation.goBack();
                  },
                },
              ],
              // {cancelable: false},
            );
          }}
          color="#fff"
          backgroundColor={REMOVE_COLOR}
          bottomText="Withdraw"
          width={cirleButtonWidth}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  modalButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
  },
});

export default NotificationModalActionButtons;
