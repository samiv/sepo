import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  BackHandler,
} from 'react-native';
import {Text} from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import RatePicker from '../Common/Fields/RatePicker';
import ParticipantCountRow from '../Common/Fields/ParticipantCountRow';
import LocationInput from '../Common/Fields/LocationInput';
import DateTimePicker from '@react-native-community/datetimepicker';
import CircleButton from '../Common/Buttons/CircleButton';
import BottomButtonRow from '../Common/Buttons/BottomButtonRow';
import {
  SCREEN_DEFAULT_PADDING,
  NETURAL_BUTTON_COLOR,
  ACCEPT_COLOR,
  commonFormStyles,
  DEFAULT_BG_COLOR,
  DEFAULT_BUTTON_WIDTH,
} from '../Common/CommonStyles';
import {SepoNotification} from '../Common/CommonTypes';
import {toFinnishDay, toLocaleTime} from '../Common/DateUtils';
import {getUserId} from '../Common/FirebaseUtils';
import {
  getDataFromAsyncStorage,
  ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
} from '../Common/AsyncStorageUtils';
import LoadingComponent from '../Common/LoadingComponent';
import SportPicker from '../Common/Fields/SportPicker';
import DurationPicker from '../Common/Fields/DurationPicker';
import {getAllRates} from '../Common/UtilFunctions';

let _ = require('lodash');

type DateType = Date | undefined;

interface NewNotificationPageProps {}

interface PropsFromState {}

interface DispatchProps {}

type AllProps = NewNotificationPageProps & PropsFromState & DispatchProps;

async function addNot(notToSave: SepoNotification) {
  const ref = firestore().collection('notifications');
  await ref.add(notToSave);
}

const CreateNotificationPage: React.FC<AllProps> = (props: AllProps) => {
  const [isFirstPage, setIsFirstPage] = useState<boolean>(true);
  const [selectedTopic, setSelectedTopic] = useState<string>('');
  const [locationText, setLocationText] = useState<string>('');
  const [selectedLowerRate, setSelectedLowerRate] = useState<number>(1);
  const [selectedHigherRate, setSelectedHigherRate] = useState<number>(1);
  const [selectedInitParticipants, setSelectedInitParticipants] = useState<
    number
  >(1);
  const [selectedMinParticipants, setSelectedMinParticipants] = useState<
    number
  >(1);
  const [selectedMaxParticipants, setSelectedMaxParticipants] = useState<
    number
  >(1);
  const [showDatePicker, setShowDatePicker] = useState<boolean>(false);
  const [dateState, setDateState] = useState<Date>(new Date());
  const [showTimePicker, setShowTimePicker] = useState<boolean>(false);
  const [selectedDuration, setSelectedDuration] = useState<string>('15min');

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const setDateFromPicker = (event: any, dateFromPicker: DateType) => {
    const dateToSet: Date = dateFromPicker || dateState;
    setShowDatePicker(Platform.OS === 'ios' ? true : false);
    setDateState(dateToSet);
  };

  const setTimeFromPicker = (event: any, timeFromPicker: DateType) => {
    const timeToSet: Date = timeFromPicker || dateState;
    setShowTimePicker(Platform.OS === 'ios' ? true : false);
    setDateState(timeToSet);
  };
  const navigation = useNavigation();
  const userId = getUserId();

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (isFirstPage) {
          return false;
        } else {
          setIsFirstPage(true);
          return true;
        }
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [isFirstPage]),
  );

  useEffect(() => {
    async function readFavouriteSport() {
      const favouriteTopic = await getDataFromAsyncStorage(
        ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
      );
      if (favouriteTopic) {
        setSelectedTopic(favouriteTopic);
      }
    }

    readFavouriteSport();
  }, []);

  if (isLoading) {
    return <LoadingComponent text="Sending..." />;
  }

  return (
    <View style={styles.ownNotificationsPage}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <>
          {/* FIRST PAGE*/}
          {isFirstPage && (
            <>
              <SportPicker
                sport={selectedTopic}
                setSport={(sport: string) => setSelectedTopic(sport)}
              />

              <LocationInput
                value={locationText}
                onChange={(text: string) => setLocationText(text)}
              />

              <TouchableOpacity
                style={commonFormStyles.dateTimeRow}
                onPress={() => setShowDatePicker(true)}>
                <>
                  <Icon
                    name="calendar"
                    size={30}
                    style={styles.dateTimePickerIcon}
                  />
                  <Text style={styles.dateTimeText}>
                    {toFinnishDay(dateState)}
                  </Text>
                </>
              </TouchableOpacity>

              {showDatePicker && (
                <DateTimePicker
                  mode="date"
                  value={dateState}
                  is24Hour={true}
                  display="default"
                  onChange={(event: any, date: DateType) =>
                    setDateFromPicker(event, date)
                  }
                />
              )}

              <TouchableOpacity
                style={commonFormStyles.dateTimeRow}
                onPress={() => setShowTimePicker(true)}>
                <>
                  <Icon
                    name="clock-o"
                    size={30}
                    style={styles.dateTimePickerIcon}
                  />
                  <Text style={styles.dateTimeText}>
                    {toLocaleTime(dateState)}
                  </Text>
                </>
              </TouchableOpacity>

              {showTimePicker && (
                <DateTimePicker
                  mode="time"
                  value={dateState}
                  is24Hour={true}
                  display="default"
                  onChange={(event: any, date: DateType) =>
                    setTimeFromPicker(event, date)
                  }
                />
              )}

              <DurationPicker
                duration={selectedDuration}
                setDuration={(d: string) => setSelectedDuration(d)}
              />
            </>
          )}
          {/* SECOND PAGE*/}
          {!isFirstPage && (
            <>
              <Text style={styles.pickerHeader}>Select Lower Rate</Text>

              <View style={{marginBottom: 8}}>
                <RatePicker
                  value={selectedLowerRate}
                  setMethod={(value: number) => {
                    setSelectedLowerRate(value);
                    if (value > selectedHigherRate) {
                      setSelectedHigherRate(value);
                    }
                  }}
                />
              </View>

              <Text style={styles.pickerHeader}>Select Upper Rate</Text>
              <View>
                <RatePicker
                  value={selectedHigherRate}
                  setMethod={(value: number) => {
                    setSelectedHigherRate(value);
                    if (value < selectedLowerRate) {
                      setSelectedLowerRate(value);
                    }
                  }}
                />
              </View>

              <Text style={styles.attendeesText}>Attendees</Text>
              <View style={styles.participantRow}>
                <ParticipantCountRow
                  label="Initial"
                  value={selectedInitParticipants}
                  setMethod={(value: number) => {
                    setSelectedInitParticipants(value);
                    if (selectedMinParticipants < value) {
                      setSelectedMinParticipants(value);
                    }
                    if (selectedMaxParticipants < value) {
                      setSelectedMaxParticipants(value);
                    }
                  }}
                />
              </View>
              <View style={styles.participantRow}>
                <ParticipantCountRow
                  label="Min (Total)"
                  value={selectedMinParticipants}
                  setMethod={(value: number) => {
                    setSelectedMinParticipants(value);
                    if (selectedMaxParticipants < value) {
                      setSelectedMaxParticipants(value);
                    }
                    if (selectedInitParticipants > value) {
                      setSelectedInitParticipants(value);
                    }
                  }}
                />
              </View>
              <View style={styles.participantRow}>
                <ParticipantCountRow
                  label="Max (Total)"
                  value={selectedMaxParticipants}
                  setMethod={(value: number) => {
                    setSelectedMaxParticipants(value);
                    if (selectedMinParticipants > value) {
                      setSelectedMinParticipants(value);
                    }
                    if (selectedInitParticipants > value) {
                      setSelectedInitParticipants(value);
                    }
                  }}
                />
              </View>
            </>
          )}

          <View />
          {isFirstPage && (
            <View style={{marginTop: 8, alignSelf: 'center'}}>
              <CircleButton
                onPress={() => setIsFirstPage(false)}
                iconName="arrow-right"
                bottomText="next"
                backgroundColor={NETURAL_BUTTON_COLOR}
                color="#fff"
                width={DEFAULT_BUTTON_WIDTH}
                iconSize={25}
              />
            </View>
          )}
          {!isFirstPage && (
            <BottomButtonRow>
              <CircleButton
                iconName="arrow-left"
                onPress={() => setIsFirstPage(true)}
                bottomText="back"
                backgroundColor={NETURAL_BUTTON_COLOR}
                color="#fff"
                width={DEFAULT_BUTTON_WIDTH}
                iconSize={25}
              />
              <CircleButton
                iconName="sign-in"
                bottomText="Send"
                backgroundColor={ACCEPT_COLOR}
                color="#fff"
                width={DEFAULT_BUTTON_WIDTH}
                iconSize={25}
                onPress={async () => {
                  setIsLoading(true);
                  await addNot({
                    topic: selectedTopic,
                    time: dateState,
                    duration: selectedDuration,
                    rates: getAllRates(selectedLowerRate, selectedHigherRate),
                    owner: userId ? userId : 'ERROR',
                    location: locationText,
                    initParticipants: selectedInitParticipants,
                    minParticipants: selectedMinParticipants,
                    maxParticipants: selectedMaxParticipants,
                    participants: _.fill(
                      Array(selectedInitParticipants),
                      userId,
                    ),
                  });
                  const favouriteTopic = await getDataFromAsyncStorage(
                    ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
                  );
                  if (favouriteTopic) {
                    setSelectedTopic(favouriteTopic);
                  }
                  setLocationText('');
                  setSelectedInitParticipants(1);
                  setSelectedMinParticipants(1);
                  setSelectedMaxParticipants(1);
                  setSelectedLowerRate(1);
                  setSelectedHigherRate(1);
                  setIsFirstPage(true);
                  navigation.navigate('OwnNotifications');
                  setIsLoading(false);
                }}
              />
            </BottomButtonRow>
          )}
        </>
      </TouchableWithoutFeedback>

      {/* {!keyboardVisible && <View style={styles.pageCountContainer}><Text style={styles.pageCountText}>{isFirstPage ? "1" : "2"}{" / 2"}</Text></View>} */}
    </View>
  );
};

export default CreateNotificationPage;

const styles = StyleSheet.create({
  ownNotificationsPage: {
    flex: 1,
    backgroundColor: DEFAULT_BG_COLOR,
    paddingHorizontal: SCREEN_DEFAULT_PADDING + 5,
    paddingTop: 32,
  },
  pickerHeader: {
    marginBottom: 1,
    fontSize: 16,
  },
  dateTimePickerIcon: {
    marginLeft: 10,
  },
  dateTimeText: {
    marginLeft: 20,
  },
  attendeesText: {
    marginTop: 16,
    marginLeft: 8,
    fontSize: 18,
  },
  participantRow: {
    marginBottom: 3,
  },
  pageCountContainer: {
    alignItems: 'center',
    marginBottom: 4,
  },
  pageCountText: {
    fontSize: 17,
    fontWeight: 'bold',
  },
});
