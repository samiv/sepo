import React from 'react';
import {ListItem, Text, Input} from 'react-native-elements';
import TouchableScale from 'react-native-touchable-scale';
import LinearGradient from 'react-native-linear-gradient';
import {SepoNotification, Sport} from '../Common/CommonTypes';
import {View, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  USER_ICON_COLOR,
  STAR_ICON_COLOR,
  APP_MAIN_COLOR,
  FIELD_BORDER_RADIUS,
  TODAY_START_COLOR,
  TODAY_END_COLOR,
} from '../Common/CommonStyles';
import {getRateString} from '../Common/UtilFunctions';
import {getDayString, toFinnishDay, toLocaleTime} from '../Common/DateUtils';
import {getImagePath} from '../Common/CommonVariables';
const _ = require('lodash');

interface NotificationListItemProps {
  item: SepoNotification;
  index: number;
  itemCount: number;
  today: boolean;
  onPress(): void;
}

interface PropsFromState {}

interface DispatchProps {}

const mapDispatchToProps: DispatchProps = {};

type AllProps = NotificationListItemProps & PropsFromState & DispatchProps;

const NotificationListItem: React.FC<AllProps> = (props: AllProps) => {
  const imagePath = getImagePath(props.item.topic);

  if (props.today) {
    return (
      <ListItem
        Component={TouchableScale}
        activeScale={0.98}
        ViewComponent={LinearGradient}
        linearGradientProps={{
          colors: [TODAY_START_COLOR, TODAY_END_COLOR],
          // start: {x: 0.6, y: 0},
          // end: {x: 0.1, y: 0},
        }}
        title={<TitleRow item={props.item} />}
        subtitle={<SubTitleRow item={props.item} />}
        containerStyle={
          styles({
            index: props.index,
            itemCount: props.itemCount,
          }).notificationListItem
        }
        onPress={() => {
          props.onPress();
        }}
        leftIcon={<Image source={imagePath} style={{width: 35, height: 35}} />}
        chevron
        chevronColor="black"
        bottomDivider
      />
    );
  } else {
    return (
      <ListItem
        Component={TouchableScale}
        activeScale={0.97}
        ViewComponent={LinearGradient}
        linearGradientProps={{
          colors: ['#dae8bc', APP_MAIN_COLOR],
          start: {x: 0.75, y: 0},
          end: {x: 0, y: 0},
        }}
        title={<TitleRow item={props.item} />}
        subtitle={<SubTitleRow item={props.item} />}
        containerStyle={
          styles({
            index: props.index,
            itemCount: props.itemCount,
          }).notificationListItem
        }
        onPress={() => {
          props.onPress();
        }}
        leftIcon={<Image source={imagePath} style={{width: 35, height: 35}} />}
        chevron
        chevronColor="black"
        bottomDivider
      />
    );
  }
};

interface TitleRowProps {
  item: SepoNotification;
}

const TitleRow = (props: TitleRowProps) => {
  return (
    <View>
      <Text>
        {toFinnishDay(props.item.time) +
          ' (' +
          getDayString(props.item.time) +
          ')'}
      </Text>
      <Text>
        {toLocaleTime(props.item.time) + '   @'}
        {props.item.location}
      </Text>
    </View>
  );
};

const SubTitleRow = (props: TitleRowProps) => {
  return (
    <View style={titleStyles.titleRow}>
      {/* <Text style={titleStyles.titleRowItem}>
        {_.upperFirst(props.item.topic)}
      </Text> */}
      <View style={titleStyles.titleRowItem}>
        <Text>{getRateString(props.item.rates)}</Text>
        <Icon
          name="star"
          color={STAR_ICON_COLOR}
          style={titleStyles.titleRowItemIcon}
        />
      </View>
      <View style={titleStyles.titleRowItem}>
        <Text>
          {props.item.participants.length + ' / ' + props.item.maxParticipants}
        </Text>
        <Icon
          name="user"
          color={USER_ICON_COLOR}
          style={titleStyles.titleRowItemIcon}
        />
        <Text>{'(Min: ' + props.item.minParticipants + ')'}</Text>
      </View>
    </View>
  );
};

export default NotificationListItem;

interface notListItemStyleProps {
  index: number;
  itemCount: number;
}

const styles = (props: notListItemStyleProps) =>
  StyleSheet.create({
    notificationListItem: {
      borderRadius: FIELD_BORDER_RADIUS,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,

      elevation: 3,
    },
  });

const titleStyles = StyleSheet.create({
  titleRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleRowItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 16,
  },
  titleRowItemIcon: {
    marginHorizontal: 4,
  },
});
