import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text} from 'react-native-elements';
import { DEFAULT_BG_COLOR } from '../Common/CommonStyles';

interface AllProps {
  firstTextRow: string;
  secondTextRow: string;
}

const NoRequestsPage: React.FC<AllProps> = (props: AllProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{props.firstTextRow}</Text>
      <Text style={styles.text}>{props.secondTextRow}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 70,
    alignItems: 'center',    
    backgroundColor: DEFAULT_BG_COLOR,
  },
  text: {
    fontSize: 30,
  },
});

export default NoRequestsPage;
