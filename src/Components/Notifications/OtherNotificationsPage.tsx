import React, {useEffect, useState} from 'react';
import {View, ScrollView} from 'react-native';
import {Text} from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import {useNavigation} from '@react-navigation/native';
import {connect} from 'react-redux';
import NotificationListItem from './NotificationListItem';
import {
  setModalNotificationAcceptable,
  setModalNotification,
} from '../store/NotificationModal/notificationModalActions';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import {addNotificationDocToArray, getUserId} from '../Common/FirebaseUtils';
import LoadingComponent from '../Common/LoadingComponent';
import {commonListStyles} from '../Common/CommonStyles';
import {isToday, compareNotificationsByTime} from '../Common/DateUtils';
import NoRequestsPage from './NoRequestsPage';

interface OtherNotificationsProps {}

interface PropsFromState {}

interface DispatchProps {
  setModalNotificationAcceptable: typeof setModalNotificationAcceptable;
  setModalNotification: typeof setModalNotification;
}

const mapDispatchToProps: DispatchProps = {
  setModalNotificationAcceptable: setModalNotificationAcceptable,
  setModalNotification: setModalNotification,
};

interface DispatchProps {}

type AllProps = OtherNotificationsProps & PropsFromState & DispatchProps;

const OtherNotificationsPage: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  const [otherNotsToday, setOtherNotsToday] = useState<
    Array<SepoNotificationWithId>
  >([]);
  const [otherNotsRest, setOtherNotsRest] = useState<
    Array<SepoNotificationWithId>
  >([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const userId = getUserId();

  useEffect(() => {
    const ownNotificationListener = firestore()
      .collection('notifications')
      .where('participants', 'array-contains', userId)
      .onSnapshot(querySnapshot => {
        const today: Array<SepoNotificationWithId> = [];
        const rest: Array<SepoNotificationWithId> = [];
        querySnapshot.docs.map(doc => {
          if (doc.data().owner !== userId) {
            if (doc.data().participants.length < doc.data().minParticipants) {
              if (isToday(doc.data().time.toDate())) {
                addNotificationDocToArray(doc, today);
              } else {
                addNotificationDocToArray(doc, rest);
              }
            }
          }
        });
        setOtherNotsToday(today.sort(compareNotificationsByTime));
        setOtherNotsRest(rest.sort(compareNotificationsByTime));
        setIsLoading(false);
      });

    return () => ownNotificationListener();
  }, []);

  if (isLoading) {
    return <LoadingComponent text="Loading..." />;
  }

  if (otherNotsToday.length === 0 && otherNotsRest.length === 0) {
    return <NoRequestsPage firstTextRow="No Replied" secondTextRow="Request" />;
  }

  return (
    <View style={commonListStyles.container}>
      <ScrollView contentContainerStyle={commonListStyles.list}>
        {otherNotsToday.length > 0 && (
          <View style={commonListStyles.today}>
            <Text style={commonListStyles.todayText}>Today</Text>
            {otherNotsToday.map((n: SepoNotificationWithId, index: number) => {
              return (
                <View
                  style={commonListStyles.notificationListItemContainer}
                  key={index.toString()}>
                  <NotificationListItem
                    item={n}
                    index={index}
                    itemCount={otherNotsToday.length}
                    today={true}
                    onPress={() => {
                      props.setModalNotificationAcceptable(false);
                      props.setModalNotification(n);
                      navigation.navigate('NotificationModal');
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}

        {otherNotsRest.length > 0 && (
          <View style={commonListStyles.restNotificationsGroup}>
            {otherNotsRest.map((n: SepoNotificationWithId, index: number) => {
              return (
                <View
                  style={commonListStyles.notificationListItemContainer}
                  key={index.toString()}>
                  <NotificationListItem
                    key={index.toString()}
                    item={n}
                    index={index}
                    itemCount={otherNotsRest.length}
                    today={false}
                    onPress={() => {
                      props.setModalNotificationAcceptable(false);
                      props.setModalNotification(n);
                      navigation.navigate('NotificationModal');
                    }}
                  />
                </View>
              );
            })}
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(OtherNotificationsPage);
