import React from 'react';
import {Text} from 'react-native-elements';
import CheckBox from '@react-native-community/checkbox';
import {View, StyleSheet} from 'react-native';

interface AllProps {
  any: boolean;
  tomorrow: boolean;
  today: boolean;
  setAny(b: boolean): void;
  setTomorrow(b: boolean): void;
  setToday(b: boolean): void;
}

const DayFilterRow: React.FC<AllProps> = (props: AllProps) => {
  return (
    <>
      <View style={styles.timeCheckBox}>
        <CheckBox
          value={props.any}
          onValueChange={(e: boolean) => {
            props.setAny(e);
            props.setTomorrow(!e);
            props.setToday(!e);
          }}
        />
        <Text>Any Day</Text>
      </View>
      <View style={styles.timeCheckBox}>
        <CheckBox
          value={props.today}
          onValueChange={(e: boolean) => {
            props.setToday(e);
            if (e) {
              props.setAny(false);
            }
            if (!e && !props.tomorrow) {
              props.setAny(true);
            }
          }}
        />
        <Text>Today</Text>
      </View>
      <View style={styles.timeCheckBox}>
        <CheckBox
          value={props.tomorrow}
          onValueChange={(e: boolean) => {
            props.setTomorrow(e);
            if (e) {
              props.setAny(false);
            }
            if (!e && !props.today) {
              props.setAny(true);
            }
          }}
        />
        <Text>Tomorrow</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  timeCheckBox: {
    alignItems: 'center',
  },
});

export default DayFilterRow;
