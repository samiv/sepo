import React, {useState} from 'react';
import {Text} from 'react-native-elements';
import {SavedSearch} from '../Common/CommonTypes';
import {connect} from 'react-redux';
import {View, StyleSheet, Alert} from 'react-native';
import {
  NETURAL_BUTTON_COLOR,
  REMOVE_COLOR,
  DEFAULT_ICON_SIZE,
  STAR_ICON_COLOR,
  USER_ICON_COLOR,
} from '../Common/CommonStyles';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import CircleButton from '../Common/Buttons/CircleButton';
import {DefaultDivider} from '../Common/UtilFunctions';
import {getSearchTimeString} from '../Common/DateUtils';
import {searchEvents} from '../Common/FirebaseUtils';
import LoadingComponent from '../Common/LoadingComponent';
import {setSearchResults} from '../store/Search/searchActions';
import SavedSearchModalRow from './SavedSearchModalRow';
import { getImagePath } from '../Common/CommonVariables';
let _ = require('lodash');

interface SavedSearchModalProps {
  item: SavedSearch;
  setVisible(visible: boolean): void;
  onDelete: (id: string) => Promise<void>;
}

interface DispatchProps {
  setSearchResults: typeof setSearchResults;
}

const mapDispatchToProps: DispatchProps = {
  setSearchResults: setSearchResults,
};

type AllProps = SavedSearchModalProps & DispatchProps;

const SavedSearchModal: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const imagePath = getImagePath(props.item.topic);

  if (isLoading) {
    return <LoadingComponent text="Loading..." />;
  }

  return (
    <>
      <View style={styles.container}>
        <View style={styles.titleRemoveIcon}>
          <Icon
            name="remove"
            size={30}
            color={REMOVE_COLOR}
            onPress={() => props.setVisible(false)}
          />
        </View>

        <View style={styles.modalSubTitleContainer}>

          <SavedSearchModalRow
            text={_.upperFirst(props.item.topic)}
            useImage={true}
            imagePath={imagePath}
          />
          <DefaultDivider />

          <SavedSearchModalRow
            text={getSearchTimeString(
              props.item.anyDay,
              props.item.today,
              props.item.tomorrow,
            )}
            iconName="calendar"
          />
          <DefaultDivider />

          <SavedSearchModalRow
            text={props.item.rate.toString()}
            iconName="star"
            iconColor={STAR_ICON_COLOR}
          />
          <DefaultDivider />

          <SavedSearchModalRow
            text={props.item.attendees.toString()}
            iconName="user"
            iconColor={USER_ICON_COLOR}
          />
        </View>
      </View>
      <View style={styles.modalButtons}>
        <CircleButton
          iconName="trash"
          onPress={() => {
            Alert.alert(
              'Remove search?',
              '',
              [
                {
                  text: 'Cancel',
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: async () => {
                    props.onDelete(props.item.id);
                    props.setVisible(false);
                  },
                },
              ],
              {cancelable: false},
            );
          }}
          color="#fff"
          backgroundColor={REMOVE_COLOR}
          bottomText="Remove"
        />
        <CircleButton
          iconName="search"
          onPress={async () => {
            setIsLoading(true);
            await searchEvents(
              props.item.topic,
              props.item.rate,
              props.item.attendees,
              props.item.anyDay,
              props.item.today,
              props.item.tomorrow,
              props.setSearchResults,
            );
            navigation.navigate('SearchResults', {backScreen: 'Saved'});
            setIsLoading(false);
            props.setVisible(false);
          }}
          color="#fff"
          backgroundColor={NETURAL_BUTTON_COLOR}
          bottomText="Apply"
        />
      </View>
    </>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(SavedSearchModal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 32,
  },
  titleRemoveIcon: {
    position: 'absolute',
    padding: 12,
    right: 0,
    top: 0,
  },
  modalSubTitleContainer: {
    marginVertical: 16,
    paddingHorizontal: 32,
  },
  iconRowStart: {
    width: 30,
    textAlign: 'center',
    marginRight: 12,
  },
  modalButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 24,
  },
});
