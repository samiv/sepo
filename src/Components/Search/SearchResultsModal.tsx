import React, {useEffect} from 'react';
import {Button, ListItem, Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {FlatList, View, StyleSheet, BackHandler} from 'react-native';
import {
  useNavigation,
  useRoute,
  useFocusEffect,
} from '@react-navigation/native';
import {
  setModalNotificationAcceptable,
  setModalNotification,
} from '../store/NotificationModal/notificationModalActions';
import {connect} from 'react-redux';
import {ApplicationState} from '../../App';
import NotificationListItem from '../Notifications/NotificationListItem';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import CustomHeader from '../Common/CustomHeader';
import {SCREEN_DEFAULT_PADDING, commonListStyles} from '../Common/CommonStyles';
import NoSearchResults from './NoSearchResults';

interface SearchResultsPageProps {}

interface PropsFromState {
  searchResults: SepoNotificationWithId[];
}

interface DispatchProps {
  setModalNotificationAcceptable: typeof setModalNotificationAcceptable;
  setModalNotification: typeof setModalNotification;
}

const mapDispatchToProps: DispatchProps = {
  setModalNotificationAcceptable: setModalNotificationAcceptable,
  setModalNotification: setModalNotification,
};

const mapStateToProps = ({search}: ApplicationState) => {
  return {
    searchResults: search.searchResults,
  };
};

type AllProps = SearchResultsPageProps & PropsFromState & DispatchProps;

const navigateBack = (nav: any, backScreen: string) => {
  backScreen ? nav.navigate(backScreen) : nav.goBack();
};

const SearchResultsModal: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  const route = useRoute();

  useEffect(() => {
    const onBackPress = () => {
      navigateBack(navigation, route.params.backScreen);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      onBackPress,
    );

    return () => backHandler.remove();
  });

  return (
    <>
      <CustomHeader
        title="Search Results"
        onBackPress={() => navigateBack(navigation, route.params.backScreen)}
      />

      {props.searchResults.length === 0 && <NoSearchResults />}

      {props.searchResults.length > 0 && (
        <View style={styles.container}>
          <FlatList
            data={props.searchResults}
            renderItem={({item}: {item: SepoNotificationWithId}) => (
              <View style={[styles.item, commonListStyles.group]}>
                <NotificationListItem
                  index={0}
                  itemCount={1}
                  today={false}
                  item={item}
                  onPress={() => {
                    props.setModalNotificationAcceptable(true);
                    props.setModalNotification(item);
                    navigation.navigate('NotificationModal');
                  }}
                />
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: SCREEN_DEFAULT_PADDING,
    marginHorizontal: SCREEN_DEFAULT_PADDING,
  },
  item: {
    marginBottom: 16,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchResultsModal);
