import React, {useCallback, useState} from 'react';
import {StyleSheet, View, FlatList, TouchableOpacity} from 'react-native';
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import {
  ASYNC_STORAGE_SAVED_SEARCHES_KEY,
  removeSearchValue,
} from '../Common/AsyncStorageUtils';
import {SavedSearch, getEmptySavedSearch} from '../Common/CommonTypes';
import SavedSearchListItem from './SavedSearchListItem';
import LoadingComponent from '../Common/LoadingComponent';
import {commonListStyles} from '../Common/CommonStyles';
import {Overlay} from 'react-native-elements';
import SavedSearchModal from './SavedSearchModal';
import {showMessage} from 'react-native-flash-message';

interface OwnNotificationsProps {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface PropsFromState {
  savedSearches: SavedSearch[];
}

interface DispatchProps {}

type AllProps = OwnNotificationsProps & PropsFromState & DispatchProps;

const SavedSearchesPage: React.FC<AllProps> = (props: AllProps) => {
  const [savedSearches, setSavedSearches] = useState<SavedSearch[]>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [overlayVisible, setOverlayVisible] = useState<boolean>(false);
  const [overlayItem, setOverlayItem] = useState<SavedSearch>(
    getEmptySavedSearch(),
  );

  useFocusEffect(
    useCallback(() => {
      readData();
    }, []),
  );

  useFocusEffect(
    useCallback(() => {
      setIsLoading(false);
    }, []),
  );

  const onDelete = async (id: string) => {
    await removeSearchValue(id);
    await readData();
    showMessage({
      message: 'Search Removed',
      type: 'danger',
      duration: 3000,
      titleStyle: {
        fontSize: 16,
      },
      icon: 'danger',
    });
  };

  const readData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem(
        ASYNC_STORAGE_SAVED_SEARCHES_KEY,
      );
      const saved = jsonValue != null ? JSON.parse(jsonValue) : null;

      if (saved !== null) {
        setSavedSearches(saved);
      }
    } catch (e) {
      alert('Failed to fetch the data from storage');
    }
  };

  if (isLoading) {
    return <LoadingComponent text="Searching..." />;
  }

  return (
    <View style={[commonListStyles.container, {paddingTop: 40}]}>
      <Overlay
        isVisible={overlayVisible}
        onBackdropPress={() => setOverlayVisible(false)}>
        <SavedSearchModal
          item={overlayItem}
          setVisible={setOverlayVisible}
          onDelete={onDelete}
        />
      </Overlay>
      <FlatList
        contentContainerStyle={commonListStyles.list}
        data={savedSearches}
        keyExtractor={(item: SavedSearch, index: number) => index.toString()}
        renderItem={({item}: {item: SavedSearch}) => (
          <TouchableOpacity
            activeOpacity={0.95}
            onPress={() => {
              setOverlayVisible(true);
              setOverlayItem(item);
            }}>
            <SavedSearchListItem item={item} />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default SavedSearchesPage;
