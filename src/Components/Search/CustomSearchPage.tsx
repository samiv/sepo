import React, {useState, useEffect, useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {setSearchResults} from '../store/Search/searchActions';
import RatePicker from '../Common/Fields/RatePicker';
import ParticipantCountRow from '../Common/Fields/ParticipantCountRow';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {
  storeSearchesToAsyncStorage,
  getDataFromAsyncStorage,
  ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
  generateSearchId,
} from '../Common/AsyncStorageUtils';
import {showMessage} from 'react-native-flash-message';
import {
  FIELD_BORDER_RADIUS,
  SCREEN_DEFAULT_PADDING,
  FIELD_BG_COLOR,
  NETURAL_BUTTON_COLOR,
  MEDIUM_FIELD_HEIGHT,
  DEFAULT_BG_COLOR,
  DEFAULT_BUTTON_WIDTH,
} from '../Common/CommonStyles';
import BottomButtonRow from '../Common/Buttons/BottomButtonRow';
import CircleButton from '../Common/Buttons/CircleButton';
import {searchEvents} from '../Common/FirebaseUtils';
import LoadingComponent from '../Common/LoadingComponent';
import SportPicker from '../Common/Fields/SportPicker';
import DayFilterRow from './DayFilterRow';

let _ = require('lodash');

interface OwnNotificationsProps {}

interface DispatchProps {
  setSearchResults: typeof setSearchResults;
}

const mapDispatchToProps: DispatchProps = {
  setSearchResults: setSearchResults,
};

interface PropsFromState {}

type AllProps = OwnNotificationsProps & PropsFromState & DispatchProps;

const CustomSearchPage: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  const [selectedTopic, setSelectedTopic] = useState<string>('');
  const [selectedRate, setSelectedRate] = useState<number>(1);
  const [selectedAttendees, setSelectedAttendees] = useState<number>(1);
  const [isToday, setIsToday] = useState<boolean>(false);
  const [isTomorrow, setIsTomorrow] = useState<boolean>(false);
  const [isAnyDay, setIsAnyDay] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    async function readFavouriteSport() {
      const favouriteTopic = await getDataFromAsyncStorage(
        ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
      );
      if (favouriteTopic) {
        setSelectedTopic(favouriteTopic);
      }
    }

    readFavouriteSport();
  }, []);

  useFocusEffect(
    useCallback(() => {
      setIsLoading(false);
    }, []),
  );

  if (isLoading) {
    return <LoadingComponent text="Searching..." />;
  }

  return (
    <View style={styles.searchPage}>
      <View style={styles.searchPageContainer}>
        <SportPicker
          sport={selectedTopic}
          setSport={(s: string) => setSelectedTopic(s)}
        />

        <View style={styles.row}>
          <ParticipantCountRow
            label="Attendee(s)"
            value={selectedAttendees}
            setMethod={(value: number) => setSelectedAttendees(value)}
          />
        </View>

        <View style={styles.row}>
          <RatePicker
            value={selectedRate}
            setMethod={(value: number) => setSelectedRate(value)}
          />
        </View>
        <View style={styles.timeCheckboxRow}>
          <DayFilterRow
            any={isAnyDay}
            today={isToday}
            tomorrow={isTomorrow}
            setAny={(b: boolean) => setIsAnyDay(b)}
            setToday={(b: boolean) => setIsToday(b)}
            setTomorrow={(b: boolean) => setIsTomorrow(b)}
          />
        </View>
      </View>

      <BottomButtonRow>
        <CircleButton
          onPress={async () => {
            setIsLoading(true);
            const newId = await generateSearchId();
            await storeSearchesToAsyncStorage({
              id: newId,
              topic: selectedTopic,
              rate: Number(selectedRate),
              attendees: selectedAttendees,
              anyDay: isAnyDay,
              today: isToday,
              tomorrow: isTomorrow,
            });
            showMessage({
              message: 'Search saved succesfully!',
              type: 'warning',
              duration: 3000,
              titleStyle: {
                fontSize: 16,
              },
              icon: 'success',
            });
            navigation.navigate('Saved');

            const favouriteTopic = await getDataFromAsyncStorage(
              ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
            );
            if (favouriteTopic) {
              setSelectedTopic(favouriteTopic);
            }
            setSelectedAttendees(1);
            setSelectedRate(1);
            setIsAnyDay(true);
            setIsToday(false);
            setIsTomorrow(false);
          }}
          iconName="save"
          bottomText="Save"
          backgroundColor={NETURAL_BUTTON_COLOR}
          color="#fff"
          width={DEFAULT_BUTTON_WIDTH}
        />
        <CircleButton
          onPress={async () => {
            setIsLoading(true);
            await searchEvents(
              selectedTopic,
              selectedRate,
              selectedAttendees,
              isAnyDay,
              isToday,
              isTomorrow,
              props.setSearchResults,
            );
            navigation.navigate('SearchResults', {backScreen: 'Custom'});
          }}
          iconName="search"
          bottomText="Apply"
          backgroundColor={NETURAL_BUTTON_COLOR}
          color="#fff"
          width={DEFAULT_BUTTON_WIDTH}
        />
      </BottomButtonRow>
    </View>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(CustomSearchPage);

const styles = StyleSheet.create({
  searchPage: {
    flex: 1,
    backgroundColor: DEFAULT_BG_COLOR,
  },
  searchPageContainer: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 6,
    // paddingTop: 30,
    // paddingHorizontal: SCREEN_DEFAULT_PADDING,
    marginTop: 24,
    marginHorizontal: SCREEN_DEFAULT_PADDING - 15,
  },
  row: {
    marginVertical: 12,
  },
  timeCheckboxRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: FIELD_BG_COLOR,
    borderRadius: FIELD_BORDER_RADIUS,
    marginVertical: 12,
    height: MEDIUM_FIELD_HEIGHT,
    elevation: 5,
    // borderWidth: FIELD_BORDER_WIDTH,
    // borderColor: FIELD_BORDER_COLOR,
  },
  timeCheckBox: {
    alignItems: 'center',
  },
});
