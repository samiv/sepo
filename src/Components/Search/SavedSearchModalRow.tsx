import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DEFAULT_ICON_SIZE} from '../Common/CommonStyles';

interface AllProps {
  text: string;
  iconName?: string;
  iconColor?: string;
  useImage?: boolean;
  imagePath?: any;
}

const SavedSearchModalRow: React.FC<AllProps> = (props: AllProps) => {
  return (
    <>
      {props.useImage && props.imagePath && (
        <View style={styles.modalRow}>
          <Image source={props.imagePath} style={{marginRight: 12, width: 35, height: 35}} />
          <Text style={styles.modalText}>{props.text}</Text>
        </View>
      )}
      {!props.useImage && props.iconName && (
        <View style={styles.modalRow}>
          <Icon
            name={props.iconName}
            size={DEFAULT_ICON_SIZE}
            style={styles.iconRowStart}
            color={props.iconColor ? props.iconColor : '#000'}
          />
          <Text style={styles.modalText}>{props.text}</Text>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  modalText: {
    fontSize: 24,
  },
  modalRow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13,
  },
  iconRowStart: {
    width: 30,
    textAlign: 'center',
    marginRight: 12,
  },
});

export default SavedSearchModalRow;
