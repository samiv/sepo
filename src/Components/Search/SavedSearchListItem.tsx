import React from 'react';
import {SavedSearch} from '../Common/CommonTypes';
import {ListItem, Text} from 'react-native-elements';
import {View, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  USER_ICON_COLOR,
  STAR_ICON_COLOR,
  FIELD_BORDER_RADIUS,
} from '../Common/CommonStyles';
import {getSearchTimeString} from '../Common/DateUtils';
import {getImagePath} from '../Common/CommonVariables';
let _ = require('lodash');

interface SavedSearchItemProps {
  item: SavedSearch;
}

type AllProps = SavedSearchItemProps;

const SavedSearchListItem: React.FC<AllProps> = (props: AllProps) => {
  const imagePath = getImagePath(props.item.topic);
  return (
    <ListItem
      title={
        <View style={styles.itemTextContainer}>
          <Text>{_.upperFirst(props.item.topic)}</Text>
          <View style={styles.countIconRow}>
            <Text style={styles.countIconRowText}>{props.item.rate}</Text>
            <Icon name="star" color={STAR_ICON_COLOR} />
          </View>
          <View style={styles.countIconRow}>
            <Text style={styles.countIconRowText}>{props.item.attendees}</Text>
            <Icon name="user" color={USER_ICON_COLOR} />
          </View>
        </View>
      }
      subtitle={getSearchTimeString(
        props.item.anyDay,
        props.item.today,
        props.item.tomorrow,
      )}
      containerStyle={styles.item}
      leftIcon={
        <View style={{width: 38, height: 38, flexDirection: 'column'}}>
          <Image source={imagePath} style={styles.image} />
          <Icon name="search" size={22} style={styles.icon} />
        </View>
      }
      bottomDivider
    />
  );
};
export default SavedSearchListItem;

const styles = StyleSheet.create({
  item: {
    borderRadius: FIELD_BORDER_RADIUS,
    marginBottom: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  countIconRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  countIconRowText: {
    marginRight: 4,
  },
  itemTextContainer: {
    flexDirection: 'row',
  },
  image: {width: 28, height: 28, opacity: 0.7},
  icon: {position: 'absolute', bottom: 5, right: 0},
});
