import React from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomHeader from '../Common/CustomHeader';
import {useNavigation} from '@react-navigation/native';

const NoSearchResults: React.FC = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Icon name="frown-o" size={50} style={{color: 'grey'}} />
      <Text
        h3
        h3Style={{
          color: 'grey',
        }}>
        No results...
      </Text>
    </View>
  );
};

export default NoSearchResults;
