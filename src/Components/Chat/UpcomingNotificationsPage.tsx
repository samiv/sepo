import React, {useEffect, useState} from 'react';
import {View, ScrollView} from 'react-native';
import {Text} from 'react-native-elements';
import {connect} from 'react-redux';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import firestore from '@react-native-firebase/firestore';
import {setOwnNotificationCount} from '../store/OwnNotifications/ownNotificationsActions';
import {addNotificationDocToArray, getUserId} from '../Common/FirebaseUtils';
import UpcomingChatListItem from './UpcomingChatListItem';
import LoadingComponent from '../Common/LoadingComponent';
import {commonListStyles} from '../Common/CommonStyles';
import {isToday, compareNotificationsByTime} from '../Common/DateUtils';
import NoRequestsPage from '../Notifications/NoRequestsPage';

const _ = require('lodash');

interface OwnNotificationsProps {}

interface PropsFromState {
  notModalVisible: boolean;
}

interface DispatchProps {
  setOwnNotificationCount: typeof setOwnNotificationCount;
}

const mapDispatchToProps: DispatchProps = {
  setOwnNotificationCount: setOwnNotificationCount,
};

type AllProps = OwnNotificationsProps & PropsFromState & DispatchProps;

const UpcomingNotificationsPage: React.FC<AllProps> = (props: AllProps) => {
  const [upcomingToday, setUpcomingToday] = useState<SepoNotificationWithId[]>(
    [],
  );
  const [upcomingRest, setUpcomingRest] = useState<SepoNotificationWithId[]>(
    [],
  );
  const userId = getUserId();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    async function getUpcomingNots() {
      await firestore()
        .collection('notifications')
        .where('participants', 'array-contains', userId)
        .onSnapshot(querySnapshot => {
          const today: Array<SepoNotificationWithId> = [];
          const rest: Array<SepoNotificationWithId> = [];
          querySnapshot.docs.map(doc => {
            if (doc.data().participants.length >= doc.data().minParticipants) {
              if (isToday(doc.data().time.toDate())) {
                addNotificationDocToArray(doc, today);
              } else {
                addNotificationDocToArray(doc, rest);
              }
            }
          });
          setUpcomingToday(today.sort(compareNotificationsByTime));
          setUpcomingRest(rest.sort(compareNotificationsByTime));
          setIsLoading(false);
          props.setOwnNotificationCount(today.length);
        });
    }
    getUpcomingNots();
  }, []);

  if (isLoading) {
    return <LoadingComponent text="Loading..." />;
  }

  if (upcomingToday.length === 0 && upcomingRest.length === 0) {
    return <NoRequestsPage firstTextRow="No Upcoming" secondTextRow="Events" />;
  }

  return (
    <View style={commonListStyles.container}>
      <ScrollView contentContainerStyle={commonListStyles.list}>
        {upcomingToday.length > 0 && (
          <View style={commonListStyles.today}>
            <Text style={commonListStyles.todayText}>Today</Text>
            <View style={[commonListStyles.group, commonListStyles.todayGroup]}>
              {upcomingToday.map((n: SepoNotificationWithId, index: number) => {
                return (
                  <UpcomingChatListItem
                    key={index.toString()}
                    index={index}
                    item={n}
                    itemCount={upcomingToday.length}
                    today={true}
                  />
                );
              })}
            </View>
          </View>
        )}
        {upcomingRest.length > 0 && (
          <View style={[commonListStyles.group, commonListStyles.restGroup]}>
            {upcomingRest.map((n: SepoNotificationWithId, index: number) => {
              return (
                <UpcomingChatListItem
                  key={index.toString()}
                  index={index}
                  item={n}
                  itemCount={upcomingRest.length}
                  today={false}
                />
              );
            })}
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(UpcomingNotificationsPage);
