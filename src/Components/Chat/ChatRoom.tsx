import React, {useState, useEffect} from 'react';
import {View, StyleSheet, BackHandler} from 'react-native';
import {Text} from 'react-native-elements';
import {
  GiftedChat,
  IMessage,
  Send,
  Bubble,
  Time,
} from 'react-native-gifted-chat';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation, useRoute} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
import {APP_MAIN_COLOR, APP_SECONDARY_COLOR} from '../Common/CommonStyles';
import {
  getUserId,
  getUser,
  getUserDisplayame,
  getUserEmail,
} from '../Common/FirebaseUtils';
import CustomHeader from '../Common/CustomHeader';

interface PropsFromState {}

type AllProps = PropsFromState;

const ChatRoom: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  const route = useRoute();
  const chatId = route.params.id;
  const currentUser = getUser();
  const currentUserId: string = getUserId();
  const [messages, setMessages] = useState<IMessage[]>([]);

  useEffect(() => {
    const onBackPress = () => {
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      onBackPress,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (chatId) {
      const chatMessageListener = firestore()
        .collection('notifications')
        .doc(chatId)
        .collection('messages')
        .orderBy('createdAt', 'desc')
        .onSnapshot(querySnapshot => {
          const queriedMessages = querySnapshot.docs.map(doc => {
            const firebaseData = doc.data();

            const data = {
              _id: doc.id,
              text: '',
              createdAt: new Date().getTime(),
              ...firebaseData,
            };

            return data;
          });

          setMessages(queriedMessages);
        });

      return () => chatMessageListener();
    }
  }, []);

  // helper method that is sends a message
  async function handleSend(newMessage: IMessage[]) {
    const text = newMessage[0].text;

    firestore()
      .collection('notifications')
      .doc(chatId)
      .collection('messages')
      .add({
        text,
        createdAt: new Date().getTime(),
        user: {
          _id: getUserId(),
          email: getUserEmail(),
          name: getUserDisplayame(),
        },
      });

    setMessages(GiftedChat.append(messages, newMessage));
  }

  return (
    <View style={styles.chatRoomContainer}>
      <CustomHeader title={route.params ? route.params.title : 'Event'} />
      <GiftedChat
        messages={messages}
        onSend={(newMessage: IMessage[]) => handleSend(newMessage)}
        user={{_id: currentUserId, name: getUserDisplayame()}}
        renderSend={renderSend}
        scrollToBottomComponent={scrollToBottomComponent}
        renderBubble={renderBubble}
        // renderTime={renderTime}
        scrollToBottom
        alwaysShowSend
        renderUsernameOnMessage
      />
    </View>
  );
};

export default ChatRoom;

const styles = StyleSheet.create({
  chatRoomContainer: {
    flex: 1,
  },
  sendingContainer: {
    marginRight: 6,
    marginBottom: 2,
  },
});

function renderBubble(props) {
  return (
    <Bubble
      {...props}
      wrapperStyle={{
        right: {
          backgroundColor: APP_SECONDARY_COLOR,
          padding: 2,
        },
        left: {
          backgroundColor: 'grey',
        },
      }}
      textStyle={{
        right: {
          color: 'white',
          fontSize: 18,
        },
        left: {
          color: 'black',
        },
      }}
    />
  );
}

function renderSend(props) {
  return (
    <Send {...props}>
      <View style={styles.sendingContainer}>
        <Icon name="arrow-circle-right" size={40} color={APP_MAIN_COLOR} />
      </View>
    </Send>
  );
}

function scrollToBottomComponent() {
  return (
    <View>
      <Icon name="angle-double-down" size={30} color={APP_SECONDARY_COLOR} />
    </View>
  );
}
