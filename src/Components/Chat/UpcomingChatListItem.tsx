import React from 'react';
import {ListItem, Text} from 'react-native-elements';
import {SepoNotificationWithId} from '../Common/CommonTypes';
import TouchableScale from 'react-native-touchable-scale';
import {StyleSheet, View} from 'react-native';
import {getSepoNotificationTitle} from '../Common/UtilFunctions';
import {getImagePath} from '../Common/CommonVariables';
import {useNavigation} from '@react-navigation/native';
import {
  FIELD_BORDER_RADIUS,
  TODAY_START_COLOR,
  TODAY_END_COLOR,
} from '../Common/CommonStyles';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import {toLocaleTime} from '../Common/DateUtils';

interface NotificationListItemProps {
  item: SepoNotificationWithId;
  index: number;
  itemCount: number;
  today: boolean;
}

interface PropsFromState {}

interface DispatchProps {}

const mapDispatchToProps: DispatchProps = {};

type AllProps = NotificationListItemProps & PropsFromState & DispatchProps;

const UpcomingChatListItem: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();
  // const imagePath = getImagePath(props.item.topic);
  const title: string = getSepoNotificationTitle(props.item);

  if (props.today) {
    return (
      <ListItem
        Component={TouchableScale}
        activeScale={0.99}
        ViewComponent={LinearGradient}
        linearGradientProps={{
          colors: [TODAY_END_COLOR, TODAY_START_COLOR],
          start: {x: 0.6, y: 0},
          end: {x: 0.1, y: 0},
        }}
        key={props.index.toString()}
        title={title}
        subtitle={
          props.item.location +
          ', ' +
          toLocaleTime(props.item.time) +
          ' (' +
          props.item.duration +
          ')'
        }
        onPress={() => {
          navigation.navigate('Chatroom', {title: title, id: props.item.id});
        }}
        leftIcon={<Icon name="comment" size={30} />}
        // leftIcon={<Image source={imagePath} style={{width: 35, height: 35}} />}
        containerStyle={
          styles({
            index: props.index,
            itemCount: props.itemCount,
          }).chatListItem
        }
        bottomDivider
        chevron
      />
    );
  } else {
    return (
      <ListItem
        Component={TouchableScale}
        activeScale={0.99}
        key={props.index.toString()}
        title={getSepoNotificationTitle(props.item)}
        subtitle={
          <View>
            <Text>{props.item.location}</Text>
            <Text>
              {toLocaleTime(props.item.time) + ' (' + props.item.duration + ')'}
            </Text>
          </View>
        }
        onPress={() => {
          navigation.navigate('Chatroom', {title: title, id: props.item.id});
        }}
        leftIcon={<Icon name="comment" size={30} />}
        // leftIcon={<Image source={imagePath} style={{width: 35, height: 35}} />}
        containerStyle={
          styles({
            index: props.index,
            itemCount: props.itemCount,
          }).chatListItem
        }
        bottomDivider
        chevron
      />
    );
  }
};

interface chatItemStyleProps {
  index: number;
  itemCount: number;
}

const styles = (props: chatItemStyleProps) =>
  StyleSheet.create({
    chatListItem: {
      borderTopLeftRadius: props.index === 0 ? FIELD_BORDER_RADIUS : 0,
      borderTopRightRadius: props.index === 0 ? FIELD_BORDER_RADIUS : 0,
      borderBottomLeftRadius:
        props.index + 1 === props.itemCount ? FIELD_BORDER_RADIUS : 0,
      borderBottomRightRadius:
        props.index + 1 === props.itemCount ? FIELD_BORDER_RADIUS : 0,
    },
  });

export default UpcomingChatListItem;
