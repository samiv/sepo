import {Reducer} from 'redux';
import {SearchState} from './searchTypes';
import {SET_SEARCH_RESULTS} from './searchActions';

const initialState: SearchState = {
  searchResults: [],
};

export const searchReducer: Reducer<SearchState> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case SET_SEARCH_RESULTS:
      return {
        ...state,
        searchResults: action.payload,
      };
    default:
      return state;
  }
};
