import { SavedSearch, SepoNotificationWithId } from './../../Common/CommonTypes';

export interface SearchState {
  searchResults: SepoNotificationWithId[];
}
