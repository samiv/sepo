import {SavedSearch, SepoNotificationWithId} from './../../Common/CommonTypes';
import {action} from 'typesafe-actions';

export const SET_SEARCH_RESULTS = 'SET_SEARCH_RESULTS';

export const setSearchResults = (results: SepoNotificationWithId[]) =>
  action(SET_SEARCH_RESULTS, results);
