export interface AppState {
    isLoading: boolean;
    isFinnish: boolean;
    isDarkMode: boolean;
    favouriteSports: string[];
}