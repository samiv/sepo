import { action } from 'typesafe-actions';

export const NAV_VISIBLE = 'NAV_VISIBLE';

export const setNavVisible = (visible: boolean) => action(NAV_VISIBLE, visible);
