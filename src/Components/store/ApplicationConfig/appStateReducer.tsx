import {
  SET_IS_LOADING,
  SET_IS_FINNISH,
  SET_IS_DARKMODE,
  SET_FAVOURITE_SPORTS,
} from './appStateActions';
import {Reducer} from 'redux';
import {AppState} from './appStateTypes';
import {getSportNames} from '../../Common/CommonVariables';
let _ = require('lodash');

const initialAppState: AppState = {
  isLoading: true,
  isFinnish: true,
  isDarkMode: false,
  favouriteSports: [], //getSportNames(),
};

export const appStateReducer: Reducer<AppState> = (
  state = initialAppState,
  action,
) => {
  switch (action.type) {
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case SET_IS_FINNISH:
      return {
        ...state,
        isFinnish: action.payload,
      };
    case SET_IS_DARKMODE:
      return {
        ...state,
        isDarkMode: action.payload,
      };
    case SET_FAVOURITE_SPORTS:
      let arr: string[] = action.payload;
      return {
        ...state,
        favouriteSports: arr.slice(),
      };
    default:
      return state;
  }
};
