import {NAV_VISIBLE} from './headerActions';
import {Reducer} from 'redux';
import { HeaderState } from './headerTypes';

const initialHeaderState: HeaderState = {
  navVisible: false,
};

export const headerReducer: Reducer<HeaderState> = (
  state = initialHeaderState,
  action,
) => {
  switch (action.type) {
    case NAV_VISIBLE:
      return {
        state,
        navVisible: action.payload,
      };
    default:
      return state;
  }
};
