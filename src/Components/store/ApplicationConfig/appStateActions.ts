import { action } from 'typesafe-actions';

export const SET_IS_LOADING = 'SET_IS_LOADING';
export const SET_IS_FINNISH = 'SET_IS_FINNISH';
export const SET_IS_DARKMODE = 'SET_IS_DARKMODE';
export const SET_FAVOURITE_SPORTS = 'SET_FAVOURITE_SPORTS';
export const REMOVE_FAVOURITE_SPORT = 'REMOVE_FAVOURITE_SPORT';

export const setIsLoading = (loading: boolean) => action(SET_IS_LOADING, loading);
export const setIsFinnish = (isFinnish: boolean) => action(SET_IS_FINNISH, isFinnish);
export const setIsDarkMode = (isDarkMode: boolean) => action(SET_IS_DARKMODE, isDarkMode);
export const setFavouriteSports = (sports: string[]) => action(SET_FAVOURITE_SPORTS, sports);
export const removeFavouriteSport = (sport: string) => action(REMOVE_FAVOURITE_SPORT, sport);