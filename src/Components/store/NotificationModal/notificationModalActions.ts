import {SepoNotificationWithId} from './../../Common/CommonTypes';
import {action} from 'typesafe-actions';

export const SET_MODAL_NOTIFICATION = 'SET_MODAL_NOTIFICATION';
export const SET_MODAL_NOTIFICATION_ACCEPTABLE =
  'SET_MODAL_NOTIFICATION_ACCEPTABLE';

export const setModalNotification = (notific: SepoNotificationWithId) =>
  action(SET_MODAL_NOTIFICATION, notific);
export const setModalNotificationAcceptable = (accpetable: boolean) =>
  action(SET_MODAL_NOTIFICATION_ACCEPTABLE, accpetable);
