import {Reducer} from 'redux';
import {
  NotificationModalState,
  getEmptyAppNotification,
} from './notificationModalTypes';
import { SET_MODAL_NOTIFICATION, SET_MODAL_NOTIFICATION_ACCEPTABLE } from './notificationModalActions';

const initialState: NotificationModalState = {
  accectable: false,
  openedNotification: getEmptyAppNotification(),
};

export const notificationModalReducer: Reducer<NotificationModalState> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case SET_MODAL_NOTIFICATION:
      return {
        ...state,
        openedNotification: action.payload,
      };
      case SET_MODAL_NOTIFICATION_ACCEPTABLE:
        return {
          ...state,
          accectable: action.payload,
        };
    default:
      return state;
  }
};
