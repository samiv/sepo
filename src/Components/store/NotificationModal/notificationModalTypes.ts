import { SepoNotificationWithId } from './../../Common/CommonTypes';

export interface NotificationModalState {
  accectable: boolean;
  openedNotification: SepoNotificationWithId;
}

export const getEmptyAppNotification = (): SepoNotificationWithId => {
  return {
    id: "EMPTY_NOTIFICATION",
    topic: "initialtopic",
    rates: [],
    time: new Date(),
    duration: "1h",
    location: "paikka",
    participants: [],
    initParticipants: 1,
    minParticipants: 1,
    maxParticipants: 1,
    owner: "",
  }
}
