import {Reducer} from 'redux';
import {OwnNotificationsState} from './ownNotificationsTypes';
import {
  SHOULD_UPDATE_OWN_NOTIFICATIONS,
  OWN_NOTIFICATION_COUNT,
} from './ownNotificationsActions';

const initialState: OwnNotificationsState = {
  shouldUpdate: false,
  ownNotificationCount: 1,
};

export const ownNotificationsReducer: Reducer<OwnNotificationsState> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case SHOULD_UPDATE_OWN_NOTIFICATIONS:
      return {
        ...state,
        shouldUpdate: action.payload,
      };
    case OWN_NOTIFICATION_COUNT:
      return {
        ...state,
        ownNotificationCount: action.payload,
      };
    default:
      return state;
  }
};
