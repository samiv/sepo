import { action } from 'typesafe-actions';

export const SHOULD_UPDATE_OWN_NOTIFICATIONS = "SHOULD_UPDATE_OWN_NOTIFICATIONS";
export const OWN_NOTIFICATION_COUNT = "OWN_NOTIFICATION_COUNT";

export const shouldUpdateOwnNotifications = (shouldUpdate: boolean) => action(SHOULD_UPDATE_OWN_NOTIFICATIONS, shouldUpdate);
export const setOwnNotificationCount = (count: number) => action(OWN_NOTIFICATION_COUNT, count);