export interface OwnNotificationsState {
  shouldUpdate: boolean;
  ownNotificationCount: number;
}
