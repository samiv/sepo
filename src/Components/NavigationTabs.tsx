import React, {useState, useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  createMaterialTopTabNavigator,
  MaterialTopTabBarOptions,
} from '@react-navigation/material-top-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import {View, Text} from 'react-native';
import OwnNotificationsPage from './Notifications/OwnNotificationsPage';
import UpcomingNotificationsPage from './Chat/UpcomingNotificationsPage';
import CreateNotificationPage from './Notifications/CreateNotificationPage';
import CustomSearchPage from './Search/CustomSearchPage';
import SavedSearchesPage from './Search/SavedSearchesPage';
import {ApplicationState} from '../App';
import {connect} from 'react-redux';
import {
  APP_MAIN_COLOR,
  HEADER_TEXT_COLOR,
  TOP_TABS_BG_COLOR,
  APP_SECONDARY_COLOR,
  TOP_TABS_ACTIVE_TINT_COLOR,
  TOP_TABS_TEXT_COLOR,
  BOTTOM_TAB_ACTIVE_BG,
} from './Common/CommonStyles';
import auth from '@react-native-firebase/auth';
import LoginPage from './Authentication/LoginPage';
import SingUpPage from './Authentication/SignUpPage';
import {getHeaderStyles} from './Common/UtilFunctions';
import OtherNotificationsPage from './Notifications/OtherNotificationsPage';
import {useKeyboardVisible} from './Common/useKeyboardVisible';
import SettingsPage from './Settings/SettingsPage';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const LoginNavigationStack = createStackNavigator();

const SettingsNavigationStack = createStackNavigator();
const BottomTab = createBottomTabNavigator();
const NotificationsTopTab = createMaterialTopTabNavigator();
const SearchNavigationStack = createStackNavigator();
const CreateNotificationsNavigationStack = createStackNavigator();
const NotificationsNavigationStack = createStackNavigator();
const ActiveNotificationsNavigationStack = createStackNavigator();
const SearchTopTab = createMaterialTopTabNavigator();

const bottomBarIconSize: number = 23;

export const customTitleStyles = (title: string) => {
  return {
    title: title,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    headerStyle: {
      backgroundColor: APP_MAIN_COLOR,
    },
    headerTintColor: HEADER_TEXT_COLOR,
  };
};

interface PropsFromState {
  ownNotificationCount: number;
  isDarkMode: boolean;
}

const mapStateToProps = ({ownNots, appState}: ApplicationState) => {
  return {
    ownNotificationCount: ownNots.ownNotificationCount,
    isDarkMode: appState.isDarkMode,
  };
};

export const AllTabs = connect(mapStateToProps)((props: PropsFromState) => {
  const [initializing, setInitializing] = useState<boolean>(true);
  const [user, setUser] = useState();
  const headerStylesWithIcons = getHeaderStyles(true);

  function onAuthStateChanged(user: any) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing)
    return (
      <View>
        <Text>Loading...</Text>
      </View>
    );

  if (!user) {
    return (
      <LoginNavigationStack.Navigator screenOptions={{headerShown: false}}>
        <LoginNavigationStack.Screen
          name="Login"
          options={headerStylesWithIcons}
          component={LoginPage}
        />
        <LoginNavigationStack.Screen
          name="Signup"
          options={headerStylesWithIcons}
          component={SingUpPage}
        />
      </LoginNavigationStack.Navigator>
    );
  }

  return (
    <BottomTabs
      eventCount={props.ownNotificationCount}
      isDarkMode={props.isDarkMode}
    />
  );
});

interface BottomTabsProps extends DarkModeProps {
  eventCount: number;
}

interface DarkModeProps {
  isDarkMode: boolean;
}

function BottomTabs(props: BottomTabsProps) {
  const keyboardVisible: boolean = useKeyboardVisible();
  const navigation = useNavigation();
  const bgColorCreate: string = props.isDarkMode ? '#000' : '#fff';
  return (
    <BottomTab.Navigator
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: props.isDarkMode ? '#fff' : APP_SECONDARY_COLOR,
        activeBackgroundColor: props.isDarkMode
          ? '#40730b'
          : BOTTOM_TAB_ACTIVE_BG,
        inactiveBackgroundColor: props.isDarkMode ? '#000' : 'transparent',
        // showLabel: false,
      }}
      initialRouteName="OwnNotifications">
      <BottomTab.Screen
        name="OwnNotifications"
        component={NotificationsNavigator}
        initialParams={{update: false}}
        options={{
          tabBarLabel: 'Pending',
          tabBarIcon: ({focused, color, size}) => (
            <Icon name="list-alt" color={color} size={bottomBarIconSize} />
          ),
        }}
      />
      <BottomTab.Screen
        name="ActiveNotifications"
        component={UpcomingNotifications}
        options={{
          title: 'Upcoming',
          tabBarIcon: ({color}) => (
            <IconWithBadge
              name="comments"
              badgeCount={props.eventCount}
              size={bottomBarIconSize}
              color={color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Create"
        component={CreateNotificationNavigator}
        options={{
          tabBarLabel: 'Create',
          tabBarIcon: ({focused, color}) => (
            <TouchableOpacity
              onPress={() => navigation.navigate('Create')}
              activeOpacity={0.6}>
              <View
                style={
                  !keyboardVisible && {
                    marginBottom: 32,
                    backgroundColor: bgColorCreate,
                    width: 52,
                    height: 52,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 40,
                    borderWidth: 1,
                    borderColor: '#dedede',
                  }
                }>
                <Icon
                  name="plus"
                  size={32}
                  color={props.isDarkMode && focused ? '#40730b' : color}
                />
              </View>
            </TouchableOpacity>
          ),
        }}
      />
      <BottomTab.Screen
        name="Search"
        component={SearchStack}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({color}) => (
            <Icon name="search" color={color} size={bottomBarIconSize} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Settings"
        component={SettingsNotificationNavigator}
        options={{
          title: 'Settings',
          tabBarIcon: ({color}) => (
            <Icon name="cog" color={color} size={bottomBarIconSize} />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

function CreateNotificationNavigator(props: DarkModeProps) {
  const headerStyles = getHeaderStyles(true, 'Create Event');

  return (
    <CreateNotificationsNavigationStack.Navigator>
      <CreateNotificationsNavigationStack.Screen
        name="CreateNotifications"
        component={CreateNotificationPage}
        options={headerStyles}
      />
    </CreateNotificationsNavigationStack.Navigator>
  );
}

function SettingsNotificationNavigator() {
  const headerStyles = getHeaderStyles(true, 'Settings');

  return (
    <SettingsNavigationStack.Navigator>
      <SettingsNavigationStack.Screen
        name="SettingsPage"
        component={SettingsPage}
        options={headerStyles}
      />
    </SettingsNavigationStack.Navigator>
  );
}

function SearchStack() {
  const headerStyles = getHeaderStyles(true, 'Search Events');

  return (
    <SearchNavigationStack.Navigator>
      <SearchNavigationStack.Screen
        name="SearchPagesTopTab"
        options={headerStyles}
        component={SearchPageTopTabNavigator}
      />
    </SearchNavigationStack.Navigator>
  );
}

function SearchPageTopTabNavigator() {
  return (
    <SearchTopTab.Navigator tabBarOptions={topTabStyles}>
      <SearchTopTab.Screen name="Custom" component={CustomSearchPage} />
      <SearchTopTab.Screen name="Saved" component={SavedSearchesPage} />
    </SearchTopTab.Navigator>
  );
}

function NotificationsNavigator() {
  const headerStyles = getHeaderStyles(true, 'Pending Events');

  return (
    <NotificationsNavigationStack.Navigator>
      <NotificationsNavigationStack.Screen
        name="Notifications"
        component={NotficationTopTabNavigator}
        options={headerStyles}
      />
    </NotificationsNavigationStack.Navigator>
  );
}

const topTabStyles: MaterialTopTabBarOptions = {
  activeTintColor: TOP_TABS_ACTIVE_TINT_COLOR,
  inactiveTintColor: TOP_TABS_TEXT_COLOR,
  labelStyle: {fontSize: 13, fontWeight: '600'},
  style: {backgroundColor: TOP_TABS_BG_COLOR},
  indicatorStyle: {
    backgroundColor: TOP_TABS_ACTIVE_TINT_COLOR,
  },
};

function NotficationTopTabNavigator() {
  return (
    <NotificationsTopTab.Navigator tabBarOptions={topTabStyles}>
      <NotificationsTopTab.Screen
        name="OwnNotifications"
        options={{
          tabBarLabel: 'Sent',
        }}
        component={OwnNotificationsPage}
      />
      <NotificationsTopTab.Screen
        name="Replied"
        component={OtherNotificationsPage}
      />
    </NotificationsTopTab.Navigator>
  );
}

function UpcomingNotifications() {
  const headerStyles = getHeaderStyles(true, 'Upcoming Events');

  return (
    <ActiveNotificationsNavigationStack.Navigator>
      <ActiveNotificationsNavigationStack.Screen
        name="Active"
        component={UpcomingNotificationsPage}
        options={headerStyles}
      />
    </ActiveNotificationsNavigationStack.Navigator>
  );
}

interface IconWithBadgeProps {
  name: string;
  badgeCount: number;
  color: string;
  size: number;
}

function IconWithBadge({name, badgeCount, color, size}: IconWithBadgeProps) {
  return (
    <View style={{width: 24, height: 24, margin: 5}}>
      <Icon name={name} size={size} color={color} />
      {badgeCount > 0 && (
        <View
          style={{
            // On React Native < 0.57 overflow outside of parent will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -6,
            top: -3,
            backgroundColor: 'red',
            borderRadius: 6,
            width: 12,
            height: 12,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>
            {badgeCount}
          </Text>
        </View>
      )}
    </View>
  );
}
