import {useFocusEffect, useNavigation} from '@react-navigation/native';
import React, {useCallback, useEffect} from 'react';
import {
  BackHandler,
  View,
  Image,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {ListItem, Text} from 'react-native-elements';
import {Sport} from '../Common/CommonTypes';
import {getImagePath, getSportNames, SPORTS} from '../Common/CommonVariables';
import CustomHeader from '../Common/CustomHeader';
import {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';
import {ApplicationState} from '../../App';
import {
  toggleFavouriteSport,
  setFavouriteSports,
} from '../store/ApplicationConfig/appStateActions';
import FavouriteSportRow from './FavouriteSportRow';
import {
  ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
  getDataFromAsyncStorage,
} from '../Common/AsyncStorageUtils';

let _ = require('lodash');

type AllProps = PropsFromState & DispatchProps;

const getFavouriteArrayAfterClick = (
  favourites: string[],
  sportName: string,
): string[] => {
  let retVal: string[] = favourites;

  if (favourites.includes(sportName)) {
    retVal = retVal.filter(s => s !== sportName);
  } else {
    retVal.push(sportName);
  }
  return retVal;
};

interface PropsFromState {
  favouriteSports: string[];
}

const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    favouriteSports: appState.favouriteSports,
  };
};

interface DispatchProps {
  setFavouriteSports: typeof setFavouriteSports;
  toggleFavouriteSport: typeof toggleFavouriteSport;
}

const mapDispatchToProps: DispatchProps = {
  setFavouriteSports: setFavouriteSports,
  toggleFavouriteSport: toggleFavouriteSport,
};

const SelectFavouriteSport: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();

  useEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        navigation.goBack();
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <View>
      <CustomHeader title="Favourite Sports" />
      <ScrollView>
        {SPORTS.map((s: Sport, i: number) => {
          return (
            <TouchableOpacity
              onPress={() => {
                let arr: string[] = props.favouriteSports;
                if (!props.favouriteSports.includes(s.name)) {
                  arr.push(s.name);
                  props.setFavouriteSports(arr);
                } else {
                  _.remove(
                    props.favouriteSports,
                    (fs: string) => fs === s.name,
                  );
                  props.setFavouriteSports(arr);
                }
              }}
              key={i.toString()}>
              <FavouriteSportRow title={s.name} />
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectFavouriteSport);
