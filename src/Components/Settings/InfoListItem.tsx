import React, {Component, useState} from 'react';
import {FIELD_BORDER_RADIUS, settingsStyles} from '../Common/CommonStyles';
import TouchableScale from 'react-native-touchable-scale';
import {View} from 'react-native';
import {ListItem, Overlay, Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ApplicationState } from '../../App';
import { connect } from 'react-redux';

interface PropsFromState {
  isDarkMode: boolean;
}
const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    isDarkMode: appState.isDarkMode,
  };
};

const InfoListItem: React.FC<PropsFromState> = (
  props: PropsFromState,
) => {
  const [infoOverlayVisible, setInfoOverlayVisible] = useState<boolean>(false);
  return (
    <>
      <ListItem
        Component={TouchableScale}
        activeScale={0.99}
        title="Info"
        subtitle="Specific info about the application"
        containerStyle={{
          ...settingsStyles({isDarkMode: props.isDarkMode}).listItemContainer,
          borderBottomLeftRadius: FIELD_BORDER_RADIUS,
          borderBottomRightRadius: FIELD_BORDER_RADIUS,
        }}
        titleStyle={
          settingsStyles({isDarkMode: props.isDarkMode}).listItemTitle
        }
        subtitleStyle={
          settingsStyles({isDarkMode: props.isDarkMode}).listItemSubTitle
        }
        onPress={() => setInfoOverlayVisible(true)}
        leftIcon={
          <Icon
            name="info-circle"
            size={29}
            style={{marginRight: 8}}
            color={props.isDarkMode ? '#fff' : '#000'}
          />
        }
        bottomDivider
      />

      <Overlay
        isVisible={infoOverlayVisible}
        onBackdropPress={() => setInfoOverlayVisible(false)}>
        <View>
          <Text>This app uses Icons made by Freepik from www.flaticon.com</Text>
          {/* <Text>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></Text> */}
        </View>
      </Overlay>
    </>
  );
};

export default connect(mapStateToProps)(InfoListItem);
