import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';
import {ApplicationState} from '../../App';

const _ = require('lodash');

interface PropsFromState {
  favouriteSports: string[];
}

const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    favouriteSports: appState.favouriteSports,
  };
};

interface Props {
  title: string;
}

type AllProps = PropsFromState & Props;

const FavouriteSportRow: React.FC<AllProps> = (props: AllProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{_.startCase(props.title)}</Text>
      {props.favouriteSports.includes(props.title) ? (
        <Icon style={styles.iconChecked} name="check-square-o" size={25} />
      ) : (
        <Icon style={styles.iconEmpty} name="square-o" size={25} />
      )}
    </View>
  );
};

export default connect(mapStateToProps)(FavouriteSportRow);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60,
    borderBottomColor: '#e0e0e0',
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    marginLeft: 20,
  },
  iconChecked: {
    marginRight: 30,
  },
  iconEmpty: {
    marginRight: 33,
  },
});
