import React, {Component, useState} from 'react';
import {FIELD_BORDER_RADIUS, settingsStyles} from '../Common/CommonStyles';
import TouchableScale from 'react-native-touchable-scale';
import {View, Alert} from 'react-native';
import {ListItem, Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import { ApplicationState } from '../../App';
import { connect } from 'react-redux';

const logout = () => {
  auth()
    .signOut()
    .then(() => console.warn('User signed out!'));
};

interface PropsFromState {
  isDarkMode: boolean;
}
const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    isDarkMode: appState.isDarkMode,
  };
};

const LogoutListItem: React.FC<PropsFromState> = (
  props: PropsFromState,
) => {
  const navigation = useNavigation();
  return (
    <ListItem
      Component={TouchableScale}
      activeScale={0.98}
      title={
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={
              settingsStyles({isDarkMode: props.isDarkMode}).listItemTitle
            }>
            Logout
          </Text>
          <Icon
            name="sign-out"
            size={29}
            style={{marginLeft: 12}}
            color={props.isDarkMode ? '#fff' : '#000'}
          />
        </View>
      }
      containerStyle={{
        ...settingsStyles({isDarkMode: props.isDarkMode}).listItemContainer,
        borderRadius: FIELD_BORDER_RADIUS,
      }}
      titleStyle={settingsStyles({isDarkMode: props.isDarkMode}).listItemTitle}
      subtitleStyle={
        settingsStyles({isDarkMode: props.isDarkMode}).listItemSubTitle
      }
      onPress={() => {
        Alert.alert(
          'Log out?',
          'Notifications are not shown when logged out',
          [
            {
              text: 'Cancel',
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                logout();
                navigation.goBack();
              },
            },
          ],
          {cancelable: false},
        );
      }}
      bottomDivider
    />
  );
};

export default connect(mapStateToProps)(LogoutListItem);
