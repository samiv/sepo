import React from 'react';
import {FIELD_BORDER_RADIUS, settingsStyles} from '../Common/CommonStyles';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ApplicationState} from '../../App';
import {connect} from 'react-redux';
import {setIsDarkMode} from '../store/ApplicationConfig/appStateActions';
import {Switch} from 'react-native';

interface PropsFromState {
  isDarkMode: boolean;
}
const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    isDarkMode: appState.isDarkMode,
  };
};

interface DispatchProps {
  setIsDarkMode: typeof setIsDarkMode;
}

const mapDispatchToProps: DispatchProps = {
  setIsDarkMode: setIsDarkMode,
};

type AllProps = PropsFromState & DispatchProps;

const DarkModeListItem: React.FC<AllProps> = (props: AllProps) => {
  return (
    <ListItem
      title="Dark Mode"
      subtitle={props.isDarkMode ? 'Enabled' : 'Disabled'}
      containerStyle={{
        ...settingsStyles({isDarkMode: props.isDarkMode}).listItemContainer,
        borderTopLeftRadius: FIELD_BORDER_RADIUS,
        borderTopRightRadius: FIELD_BORDER_RADIUS,
      }}
      titleStyle={settingsStyles({isDarkMode: props.isDarkMode}).listItemTitle}
      subtitleStyle={
        settingsStyles({isDarkMode: props.isDarkMode}).listItemSubTitle
      }
      bottomDivider
      leftIcon={
        <Icon
          name="sun-o"
          size={25}
          style={{marginRight: 8}}
          color={props.isDarkMode ? '#fff' : '#000'}
        />
      }
      rightIcon={
        <Switch
          value={props.isDarkMode}
          onValueChange={e => props.setIsDarkMode(e)}
        />
      }
    />
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DarkModeListItem);
