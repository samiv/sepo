import React, {Component, useState} from 'react';
import {FIELD_BORDER_RADIUS, settingsStyles} from '../Common/CommonStyles';
import TouchableScale from 'react-native-touchable-scale';
import {ListItem, Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {getUserDisplayame} from '../Common/FirebaseUtils';
import {ApplicationState} from '../../App';
import { connect } from 'react-redux';

interface PropsFromState {
  isDarkMode: boolean;
}
const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    isDarkMode: appState.isDarkMode,
  };
};

const UsernameListItem: React.FC<PropsFromState> = (props: PropsFromState) => {
  const [username, setUsername] = useState<string>(getUserDisplayame());
  const [usernameEditable, setUsernameEditable] = useState<boolean>(false);
  return (
    <ListItem
      Component={TouchableScale}
      activeScale={0.99}
      containerStyle={{
        ...settingsStyles({isDarkMode: props.isDarkMode}).listItemContainer,
        borderBottomLeftRadius: FIELD_BORDER_RADIUS,
        borderBottomRightRadius: FIELD_BORDER_RADIUS,
      }}
      titleStyle={settingsStyles({isDarkMode: props.isDarkMode}).listItemTitle}
      subtitleStyle={
        settingsStyles({isDarkMode: props.isDarkMode}).listItemSubTitle
      }
      title="Username"
      subtitle={username}
      leftIcon={
        <Icon
          name="user"
          size={29}
          style={{marginRight: 8}}
          color={props.isDarkMode ? '#fff' : '#000'}
        />
      }
      chevron
      bottomDivider
    />
  );
};

export default connect(mapStateToProps)(UsernameListItem);
