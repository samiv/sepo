import React from 'react';
import {View, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {setIsFinnish} from '../store/ApplicationConfig/appStateActions';
import {ApplicationState} from '../../App';
import {connect} from 'react-redux';
import {settingsStyles} from '../Common/CommonStyles';

interface DispatchProps {
  setIsFinnish: typeof setIsFinnish;
}

const mapDispatchToProps: DispatchProps = {
  setIsFinnish: setIsFinnish,
};

interface PropsFromState {
  isFinnish: boolean;
  isDarkMode: boolean;
}

const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    isFinnish: appState.isFinnish,
    isDarkMode: appState.isDarkMode,
  };
};

type AllProps = PropsFromState & DispatchProps;

const LanguageListItem: React.FC<AllProps> = (props: AllProps) => {
  return (
    <ListItem
      title="Language"
      subtitle={props.isFinnish ? 'Suomi' : 'English'}
      bottomDivider
      containerStyle={
        settingsStyles({isDarkMode: props.isDarkMode}).listItemContainer
      }
      titleStyle={settingsStyles({isDarkMode: props.isDarkMode}).listItemTitle}
      subtitleStyle={
        settingsStyles({isDarkMode: props.isDarkMode}).listItemSubTitle
      }
      leftIcon={
        <Icon
          name="language"
          size={29}
          style={{marginRight: 8}}
          color={props.isDarkMode ? '#fff' : '#000'}
        />
      }
      rightIcon={
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={
              !props.isFinnish
                ? styles.flagContainerSelected
                : styles.flagContainer
            }
            onPress={() => props.setIsFinnish(false)}>
            <Image
              source={require('../../Assets/Images/usa-today.png')}
              style={{width: 28, height: 28}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={
              props.isFinnish
                ? styles.flagContainerSelected
                : styles.flagContainer
            }
            onPress={() => props.setIsFinnish(true)}>
            <Image
              source={require('../../Assets/Images/finland.png')}
              style={{width: 28, height: 28}}
            />
          </TouchableOpacity>
        </View>
      }
    />
  );
};

const styles = StyleSheet.create({
  flagContainer: {
    marginRight: 8,
    padding: 2,
  },
  flagContainerSelected: {
    backgroundColor: '#efe7fb',
    marginRight: 8,
    padding: 4,
    borderRadius: 2,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LanguageListItem);
