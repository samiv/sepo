import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  SCREEN_DEFAULT_PADDING,
  DARK_MODE_BG_COLOR,
  DEFAULT_BG_COLOR,
  FIELD_BORDER_RADIUS,
  DarkModeStyle,
} from '../Common/CommonStyles';
import {connect} from 'react-redux';
import {ApplicationState} from '../../App';
import InfoListItem from './InfoListItem';
import LogoutListItem from './LogoutListItem';
import FavouriteSportListItem from './FavouriteSportListItem';
import UsernameListItem from './UsernameListItem';
import DarkModeListItem from './DarkModeListItem';
import LanguageListItem from './LanguageListItem';

let _ = require('lodash');

interface PropsFromState {
  isDarkMode: boolean;
}

const mapStateToProps = ({appState}: ApplicationState) => {
  return {
    isDarkMode: appState.isDarkMode,
  };
};

type AllProps = PropsFromState;

const SettingsPage: React.FC<AllProps> = (props: AllProps) => {
  return (
    <View style={styles({isDarkMode: props.isDarkMode}).settingsPageContainer}>
      <View style={styles({isDarkMode: props.isDarkMode}).settingsGroup}>
        <FavouriteSportListItem />
        <UsernameListItem />
      </View>

      <View style={styles({isDarkMode: props.isDarkMode}).settingsGroup}>
        <DarkModeListItem />
        <LanguageListItem />
        <InfoListItem />
      </View>

      <View style={styles({isDarkMode: props.isDarkMode}).settingsGroup}>
        <LogoutListItem />
      </View>
    </View>
  );
};

export const styles = (props: DarkModeStyle) =>
  StyleSheet.create({
    settingsPageContainer: {
      flex: 1,
      paddingTop: SCREEN_DEFAULT_PADDING * 2,
      paddingHorizontal: SCREEN_DEFAULT_PADDING,
      backgroundColor: props.isDarkMode ? DARK_MODE_BG_COLOR : DEFAULT_BG_COLOR,
    },
    settingsGroup: {
      marginBottom: 16,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,

      elevation: 4,
      borderRadius: FIELD_BORDER_RADIUS,
    },
  });

export default connect(mapStateToProps)(SettingsPage);
