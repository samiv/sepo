import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {connect} from 'react-redux';
import {setIsLoading} from '../store/ApplicationConfig/appStateActions';

interface SplashScreenProps {}

interface DispatchProps {
  setIsLoading: typeof setIsLoading;
}

const mapDispatchToProps: DispatchProps = {
  setIsLoading: setIsLoading,
};

type AllProps = SplashScreenProps & DispatchProps;

function performTimeConsumingTask() {
  return new Promise((resolve, reject) =>
    setTimeout(() => {
      resolve('result');
    }, 3000),
  );
}

const SplashScreen = (props: AllProps) => {
  useEffect(() => {
    async function consumeSplashTime() {
      const data = await performTimeConsumingTask();
      if (data !== null) {
        props.setIsLoading(false);
      }
    }

    consumeSplashTime();
  }, []);

  return (
    <View style={styles.splashScreen}>
      <Text>SePo</Text>
      <Image source={require('../Assets/Images/sepo-app-icon.png')} />
    </View>
  );
};

export default connect(null, mapDispatchToProps)(SplashScreen);

const styles = StyleSheet.create({
  splashScreen: {
    flex: 1,
    backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  },
  splashScreenText: {
    color: 'black',
    fontSize: 40,
    fontWeight: 'bold',
  },
});
