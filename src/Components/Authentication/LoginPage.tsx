import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {Text, Input} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import {APP_MAIN_COLOR, FIELD_BG_COLOR} from '../Common/CommonStyles';
import LinearGradient from 'react-native-linear-gradient';
import auth from '@react-native-firebase/auth';
import {useKeyboardVisible} from '../Common/useKeyboardVisible';

interface PropsFromState {}

type AllProps = PropsFromState;

const LoginPage: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();

  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [mailErrorMessage, setMailErrorMessage] = useState<string>('');
  const [pwErrorMessage, setPwErrorMessage] = useState<string>('');
  const [submitted, setSubmitted] = useState<boolean>(false);

  const keyboardVisible = useKeyboardVisible();

  const login = (email: string, password: string) => {
    setSubmitted(true);
    if (password.length === 0 || email.length === 0) {
      setPwErrorMessage(password.length === 0 ? 'Type Password' : '');
      setMailErrorMessage(email.length === 0 ? 'Type Email Address' : '');
      return;
    }
    if (password.length < 6) {
      setPwErrorMessage('Type must be at least 6 characters');
      return;
    } else {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          console.warn('User account created & signed in!');
          setSubmitted(false);
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            setMailErrorMessage('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            setMailErrorMessage('That email address is invalid!');
          }

          console.warn(error);
        });
    }
  };

  const clearErrorMessages = () => {
    setSubmitted(false);
    setPwErrorMessage('');
    setMailErrorMessage('');
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <LinearGradient
        colors={[APP_MAIN_COLOR, 'yellow']}
        locations={[0.5, 0.9]}
        style={styles.loginPageContainer}>
        <View style={styles.mainContent}>
          <Image
            source={require('../../Assets/Images/logo.png')}
            style={keyboardVisible ? styles.logoKeyboard : styles.logo}
          />

          <Input
            containerStyle={styles.inputContainerStyle}
            placeholder="email"
            rightIcon={{type: 'font-awesome', name: 'envelope'}}
            onChangeText={(value: string) => {
              setEmail(value);
              clearErrorMessages();
            }}
            keyboardType="email-address"
            errorMessage={
              submitted && mailErrorMessage.length > 0 ? mailErrorMessage : ''
            }
          />
          <Input
            containerStyle={styles.inputContainerStyle}
            placeholder="Password"
            rightIcon={{type: 'font-awesome', name: 'lock'}}
            onChangeText={(value: string) => {
              setPassword(value);
              clearErrorMessages();
            }}
            secureTextEntry={true}
            errorMessage={
              submitted && pwErrorMessage.length > 0 ? pwErrorMessage : ''
            }
          />
          <TouchableOpacity
            onPress={() => {
              login(email, password);
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.25}}
              end={{x: 0.5, y: 1.0}}
              locations={[0, 0.5]}
              colors={['#4c669f', '#3b5998']}
              style={styles.buttonPrimary}>
              <Text style={styles.buttonText}>Login</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        {!keyboardVisible && (
          <View style={styles.additionalInfo}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Signup');
              }}>
              <LinearGradient
                locations={[0, 0.5]}
                colors={['#4c669f', '#3b5998']}
                style={styles.buttonSecondary}>
                <Text style={styles.buttonText}>Create New Account</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        )}
      </LinearGradient>
    </TouchableWithoutFeedback>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  loginPageContainer: {
    paddingVertical: 24,
    paddingHorizontal: 40,
    flex: 1,
  },
  mainContent: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    marginTop: 80,
    marginBottom: 80,
  },
  logoKeyboard: {
    width: 100,
    height: 100,
    marginTop: 24,
    marginBottom: 8,
  },
  inputContainerStyle: {
    backgroundColor: FIELD_BG_COLOR,
    marginVertical: 16,
    borderRadius: 4,
  },
  buttonPrimary: {
    width: 180,
    height: 40,
    marginTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  buttonSecondary: {
    width: 280,
    height: 40,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
  },
  additionalInfo: {
    alignItems: 'center',
  },
});
