import React, {useState} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {Text, Input, Button} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import {APP_MAIN_COLOR, FIELD_BG_COLOR} from '../Common/CommonStyles';
import LinearGradient from 'react-native-linear-gradient';
import {TouchableHighlight} from 'react-native-gesture-handler';
import CircleButton from '../Common/Buttons/CircleButton';
import auth from '@react-native-firebase/auth';
import {useKeyboardVisible} from '../Common/useKeyboardVisible';

interface PropsFromState {}

type AllProps = PropsFromState;

const SingUpPage: React.FC<AllProps> = (props: AllProps) => {
  const navigation = useNavigation();

  const [displayName, setDisplayName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [mailErrorMessage, setMailErrorMessage] = useState<string>('');
  const [pwErrorMessage, setPwErrorMessage] = useState<string>('');
  const [nameErrorMessage, setNameErrorMessage] = useState<string>('');
  const [submitted, setSubmitted] = useState<boolean>(false);

  const keyboardVisible = useKeyboardVisible();

  const signup = (email: string, password: string) => {
    setSubmitted(true);
    if (displayName.length < 2 || displayName.length > 10) {
      setNameErrorMessage('User name must include 2-10 characters');
      return;
    }
    if (password.length === 0 || email.length === 0) {
      setPwErrorMessage(password.length === 0 ? 'Type Password' : '');
      setMailErrorMessage(email.length === 0 ? 'Type Email Address' : '');
      return;
    }
    if (password.length < 6) {
      setPwErrorMessage('Type must be at least 6 characters');
      return;
    } else {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          const user = auth().currentUser;
          if (user) {
            user.updateProfile({
              displayName: displayName,
            });
          }
          console.warn('User account created & signed in!');
          setSubmitted(false);
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            setMailErrorMessage('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            setMailErrorMessage('That email address is invalid!');
          }

          console.warn(error);
        });
    }
  };

  const clearErrorMessages = () => {
    setSubmitted(false);
    setPwErrorMessage('');
    setMailErrorMessage('');
  };

  return (
    <LinearGradient
      colors={[APP_MAIN_COLOR, 'yellow']}
      locations={[0.5, 0.9]}
      style={
        keyboardVisible
          ? styles.signupPageContainerKeyboard
          : styles.sigupPageContainer
      }>
      <View style={styles.mainContent}>
        <Text style={keyboardVisible ? styles.headerKeyboard : styles.header}>
          Create New Account
        </Text>
        <Input
          containerStyle={styles.inputContainerStyle}
          placeholder="User Name"
          rightIcon={{type: 'font-awesome', name: 'user'}}
          onChangeText={(value: string) => {
            setDisplayName(value);
            clearErrorMessages();
          }}
          errorMessage={
            submitted && nameErrorMessage.length > 0 ? nameErrorMessage : ''
          }
        />
        <Input
          containerStyle={styles.inputContainerStyle}
          placeholder="email"
          rightIcon={{type: 'font-awesome', name: 'envelope'}}
          onChangeText={(value: string) => {
            setEmail(value);
            clearErrorMessages();
          }}
          keyboardType="email-address"
          errorMessage={
            submitted && mailErrorMessage.length > 0 ? mailErrorMessage : ''
          }
        />
        <Input
          containerStyle={styles.inputContainerStyle}
          placeholder="Password"
          rightIcon={{type: 'font-awesome', name: 'lock'}}
          onChangeText={(value: string) => {
            setPassword(value);
            clearErrorMessages();
          }}
          secureTextEntry={true}
          errorMessage={
            submitted && pwErrorMessage.length > 0 ? pwErrorMessage : ''
          }
        />
        <TouchableHighlight
          onPress={() => {
            signup(email, password);
          }}>
          <LinearGradient
            start={{x: 0.0, y: 0.25}}
            end={{x: 0.5, y: 1.0}}
            locations={[0, 0.5]}
            colors={['#4c669f', '#3b5998']}
            style={styles.buttonPrimary}>
            <Text style={styles.buttonText}>Create</Text>
          </LinearGradient>
        </TouchableHighlight>
      </View>

      {!keyboardVisible && (
        <View style={styles.backButton}>
          <CircleButton
            iconName="chevron-left"
            onPress={() => navigation.goBack()}
          />
        </View>
      )}
    </LinearGradient>
  );
};

export default SingUpPage;

const styles = StyleSheet.create({
  sigupPageContainer: {
    paddingTop: 160,
    paddingHorizontal: 40,
    flex: 1,
  },
  signupPageContainerKeyboard: {
    paddingTop: 32,
    paddingHorizontal: 40,
    flex: 1,
  },
  mainContent: {
    flex: 1,
    alignItems: 'center',
  },
  header: {
    marginBottom: 40,
    fontSize: 24,
  },
  headerKeyboard: {
    marginBottom: 16,
    fontSize: 24,
  },
  inputContainerStyle: {
    backgroundColor: FIELD_BG_COLOR,
    marginVertical: 10,
    borderRadius: 4,
  },
  buttonPrimary: {
    width: 180,
    height: 40,
    marginTop: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },

  buttonText: {
    fontSize: 18,
    color: '#fff',
  },

  backButton: {
    marginBottom: 24,
    alignSelf: 'center',
  },
});
