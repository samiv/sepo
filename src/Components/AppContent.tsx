import {NavigationContainer, useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useEffect} from 'react';
import {connect} from 'react-redux';
import ChatRoom from './Chat/ChatRoom';
import {AllTabs} from './NavigationTabs';
import NotificationModal from './Notifications/NotificationModal';
import SavedSearchModal from './Search/SavedSearchModal';
import SearchResultsModal from './Search/SearchResultsModal';
import SelectFavouriteSport from './Settings/SelectFavouriteSport';
import {createStackNavigator} from '@react-navigation/stack';
import {setFavouriteSports} from './store/ApplicationConfig/appStateActions';
import {
  getDataFromAsyncStorage,
  ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
} from './Common/AsyncStorageUtils';

interface DispatchProps {
  setFavouriteSports: typeof setFavouriteSports;
}

const mapDispatchToProps: DispatchProps = {
  setFavouriteSports: setFavouriteSports,
};

type AllProps = DispatchProps;

const RootStack = createStackNavigator();

const AppContent: React.FC<AllProps> = (props: AllProps) => {
  useEffect(
    useCallback(() => {
      readData();
    }, []),
  );

  const readData = async () => {
    const favSport = await getDataFromAsyncStorage(
      ASYNC_STORAGE_FAVOURITE_SPORT_KEY,
    );
    console.warn("Mounting app, Favourite Sports: " + favSport);
  };
  return (
    <NavigationContainer>
      <RootStack.Navigator mode="modal" headerMode="none">
        <RootStack.Screen name="Main" component={AllTabs} />
        {/* Chatroom sen takia täällä, koska bottomTabBar on piilotettava */}
        <RootStack.Screen name="Chatroom" component={ChatRoom} />
        {/* Search Results sen takia täällä, koska bottomTabBar on piilotettava */}
        <RootStack.Screen name="SearchResults" component={SearchResultsModal} />
        <RootStack.Screen
          name="NotificationModal"
          component={NotificationModal}
        />
        <RootStack.Screen
          name="SavedSearchModal"
          component={SavedSearchModal}
        />
        {/* Favourite sen takia täällä, koska bottomTabBar on piilotettava */}
        <RootStack.Screen name="Favourite" component={SelectFavouriteSport} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default connect(
  null,
  mapDispatchToProps,
)(AppContent);
