/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component, useCallback, useEffect} from 'react';
import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import {notificationModalReducer} from './Components/store/NotificationModal/notificationModalReducer';
import {appStateReducer} from './Components/store/ApplicationConfig/appStateReducer';
import {NotificationModalState} from './Components/store/NotificationModal/notificationModalTypes';
import {HeaderState} from './Components/store/ApplicationConfig/headerTypes';
import {AppState} from './Components/store/ApplicationConfig/appStateTypes';
import {NavigationContainer, useFocusEffect} from '@react-navigation/native';
import NotificationModal from './Components/Notifications/NotificationModal';
import {AllTabs, customTitleStyles} from './Components/NavigationTabs';
import {OwnNotificationsState} from './Components/store/OwnNotifications/ownNotificationsTypes';
import {ownNotificationsReducer} from './Components/store/OwnNotifications/ownNotificationsReducer';
import {SearchState} from './Components/store/Search/searchTypes';
import {searchReducer} from './Components/store/Search/searchReducer';
import {BackHandler, StatusBar} from 'react-native';
import FlashMessage from 'react-native-flash-message';
import {APP_SECONDARY_COLOR} from './Components/Common/CommonStyles';
import SavedSearchModal from './Components/Search/SavedSearchModal';
import ChatRoom from './Components/Chat/ChatRoom';
import SearchResultsModal from './Components/Search/SearchResultsModal';
import SelectFavouriteSport from './Components/Settings/SelectFavouriteSport';
import { getDataFromAsyncStorage, ASYNC_STORAGE_FAVOURITE_SPORT_KEY } from './Components/Common/AsyncStorageUtils';
import { getImagePath } from './Components/Common/CommonVariables';
import { setFavouriteSports } from './Components/store/ApplicationConfig/appStateActions';
import AppContent from './Components/AppContent';

let composeEnhancers: any = compose;

if (__DEV__) {
  composeEnhancers =
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

export interface ApplicationState {
  header: HeaderState;
  notModal: NotificationModalState;
  appState: AppState;
  ownNots: OwnNotificationsState;
  search: SearchState;
}

let store = createStore(
  combineReducers({
    // header: headerReducer,
    notModal: notificationModalReducer,
    appState: appStateReducer,
    ownNots: ownNotificationsReducer,
    search: searchReducer,
  }),
  composeEnhancers(applyMiddleware(ReduxThunk)),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);


interface AppProps {}

interface PropsFromState {}

type AllProps = AppProps & PropsFromState;

const App: React.FC<AllProps> = (props: AllProps) => {

  useEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <Provider store={store}>
      <StatusBar
        backgroundColor={APP_SECONDARY_COLOR}
        barStyle="light-content"
      />
      <AppContent />
      <FlashMessage position="top" />
    </Provider>
  );
};

// class App extends Component<AllProps> {
//   constructor(props: AllProps) {
//     super(props);
//   }

//   render() {
//     return (
//       <Provider store={store}>
//         <StatusBar
//           backgroundColor={APP_SECONDARY_COLOR}
//           barStyle="light-content"
//         />
//         <NavigationContainer>
//           <RootStack.Navigator mode="modal" headerMode="none">
//             <RootStack.Screen name="Main" component={AllTabs} />
//             {/* Chatroom sen takia täällä, koska bottomTabBar on piilotettava */}
//             <RootStack.Screen name="Chatroom" component={ChatRoom} />
//             {/* Search Results sen takia täällä, koska bottomTabBar on piilotettava */}
//             <RootStack.Screen
//               name="SearchResults"
//               component={SearchResultsModal}
//             />
//             {/* <RootStack.Screen name="Settings" component={SettingsPage} /> */}
//             <RootStack.Screen
//               name="NotificationModal"
//               component={NotificationModal}
//             />
//             <RootStack.Screen
//               name="SavedSearchModal"
//               component={SavedSearchModal}
//             />
//           </RootStack.Navigator>
//         </NavigationContainer>
//         <FlashMessage position="top" />
//       </Provider>
//     );
//   }
// }

export default App;
